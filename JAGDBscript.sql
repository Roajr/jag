/*creates the emaildb*/
create database if not exists emaildb;

/*creates the user for the emaildb
grant all privileges to the new user to the emaildb database*/
create user if not exists 'emailuser'@'localhost' identified by 'EmailPass';
grant all on emaildb.* to 'emailuser'@'localhost';
