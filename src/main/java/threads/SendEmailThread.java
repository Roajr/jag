package threads;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import business.DatabaseController;
import business.MailController;
import data.EmailBean;

public class SendEmailThread extends Thread{	
	private final static Logger LOG = LoggerFactory.getLogger(MailController.class);
	private DatabaseController dbManager;
	private MailController emailControl;
	private Properties prop;
	private Thread thread;
	private EmailBean email;
	
	/**
	 * thread to send email constructor
	 * @param dbManager
	 * @throws IOException
	 */
	public SendEmailThread(DatabaseController dbManager, Properties prop, EmailBean email) throws IOException {
		this.dbManager = dbManager;
		this.emailControl = new MailController();
		this.prop = prop;
		this.email = email;
	}
	
	/**
	 * tread that sends a email
	 */
	public void run() {
		try {
			LOG.debug("Sending Email");
			if(emailControl.sendEmail(prop.getProperty("sendEmail"), prop.getProperty("send"), email)) {
				LOG.debug("Email Sent adding to DB");
				dbManager.create(email);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/** 
	 * creates the new thread
	 */
	public void start() {
		if (thread == null) {
			thread = new Thread(this);
			thread.start ();
	      }
	}
}
