package threads;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import business.DatabaseController;
import business.MailController;
import data.EmailBean;

public class ReceiveEmailThread extends Thread{

    private final static Logger LOG = LoggerFactory.getLogger(MailController.class);
    private DatabaseController dbManager;
	private MailController emailControl;
	private Properties prop;
	private Thread thread;
	
	/**
	 * thread to receive email constructor
	 * @param dbManager
	 * @throws IOException
	 */
	public ReceiveEmailThread(DatabaseController dbManager) throws IOException {
		this.dbManager = dbManager;
		this.emailControl = new MailController();
		prop = new Properties();
		FileInputStream propInput = new FileInputStream("jag.properties");
		prop.load(propInput);

	}
	
	/**
	 * tread that receives emails ever 10 seconds
	 */
	public void run() {
		while(true) {
			try {
				LOG.debug("Checking for new Emails");
				List<EmailBean> receivedEmails =this.emailControl.receiveMail(prop.getProperty("sendEmail"),prop.getProperty("send"));
				for(EmailBean email : receivedEmails) {
					this.dbManager.create(email);
				}
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
		}
	}
	
	/** 
	 * creates the new thread
	 */
	public void start() {
		if (thread == null) {
			thread = new Thread(this);
			thread.start ();
	      }
	}
}
