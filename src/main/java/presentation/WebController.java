package presentation;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.Properties;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import business.DatabaseController;
import business.MailController;
import data.EmailBean;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.web.WebView;
import javafx.stage.FileChooser;

public class WebController {
    private final static Logger LOG = LoggerFactory.getLogger(MailController.class);
    private DatabaseController dbManager;
    private int showingInt;

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private AnchorPane webTable;

    @FXML
    private WebView webViewer;
    
    @FXML
    private Label subject;
    
    @FXML
    private Button attachBtn;

    @FXML
    void initialize() {
    	final String html = "example.html";
        final java.net.URI uri = java.nio.file.Paths.get(html).toAbsolutePath().toUri();
        webViewer.getEngine().load(uri.toString());
    }
    
    /**
     * downloads all attachments for the email that is in the webview
     * @throws SQLException
     */
    @FXML
    private void downloadAttach() throws SQLException {
    	Alert alert = new Alert(AlertType.INFORMATION);
    	alert.setTitle(resources.getString("folderSelect"));
    	alert.setHeaderText(null);
    	alert.setContentText(resources.getString("folderSelectDal"));

    	alert.showAndWait();
    	
    	FileChooser fileChooser = new FileChooser();
    	File dest = fileChooser.showSaveDialog(null);
    	if (dest != null) {
    		dest.mkdir();
        	this.dbManager.downloadAttachments(showingInt,dest);
    	}
    }
    
	/**
	 * Sets up the dbManager off the properties file
	 */
    public void setDbManager(DatabaseController dbManager) {
    	this.dbManager = dbManager;
    }

    /** 
     * reloads the webview
     * @param email
     */
	public void reloadWeb(EmailBean email) {
		showingInt = email.getEmailID();
		subject.setText(email.getSubject());
	}
}