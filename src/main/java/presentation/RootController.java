package presentation;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import app.Jag;
import business.DatabaseController;
import business.MailController;
import data.WebBeanFx;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import threads.ReceiveEmailThread;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RootController {
    private final static Logger LOG = LoggerFactory.getLogger(MailController.class);

    private DatabaseController dbManager;
    private Properties prop;
    private InputStream propInput;
    private TreeController treeControl;
    public TableController tableControl;
    public WebController webControl;
    private HtmlController htmlControl;
    private Jag jag;
    private int lang = 1;
    
    @FXML
    private ResourceBundle resources;
    
    @FXML
    private AnchorPane topLeft;
    
    @FXML
    private AnchorPane topRight;
    
    @FXML
    private AnchorPane bottomLeft;
    
    @FXML
    private AnchorPane bottomRight;
    
    /**
     * inits the root control 
     * @throws IOException
     * @throws SQLException
     */
    @FXML
    private void initialize() throws IOException, SQLException{
    	initTopLeft();
    	initTopRight();
    	initBottomLeft();
    	initBottomRight();
    	
		try {
			this.treeControl.displayTree();
			this.tableControl.displayTheTable();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		ReceiveEmailThread receiveThread = new ReceiveEmailThread(this.dbManager);
		receiveThread.start();
    }
    
    /**
     * closes the app
     */
    @FXML
    private void close() {
    	System.exit(0);
    }
    
    /**
     * switches the lang of the app
     * @throws IOException
     * @throws SQLException
     * @throws InterruptedException
     */
    @FXML
    private void lanSwitch() throws IOException, SQLException, InterruptedException {
    	if(lang == 1) {
        	this.jag.setLocal(new Locale("fr", "CA"));

    	}else {
        	this.jag.setLocal(new Locale("en","CA"));
    	}
    	jag.loadMainApp();
    }
    
    /**
     * reads the readme file and tells you how to use the app
     * @throws IOException
     */
    @FXML
    private void about() throws IOException {
    	Alert alert = new Alert(AlertType.INFORMATION);
    	alert.setTitle(resources.getString("about"));
    	alert.setHeaderText(null);
    	byte[] text = Files.readAllBytes(Paths.get("README.md"));
    	alert.setContentText(new String(text));

    	alert.showAndWait();
    }
    
    /**
     * sets up the main window
     */
    public RootController() {
    	super();
    	try {
			prop = new Properties();
			this.propInput = new FileInputStream("jag.properties");
			this.prop.load(propInput);
			this.dbManager = new DatabaseController(prop);
			
    	} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			LOG.debug("Cannot read properties file");
			e.printStackTrace();
		}
    }
    
    /**
     * sets up the tree view
     */
    private void initTopLeft() {
    	try {
    		FXMLLoader loader = new FXMLLoader();
            loader.setResources(resources);
            loader.setLocation(getClass().getResource("/fxml/treeLayout.fxml"));
    		AnchorPane treeView = loader.load();
            loader.setResources(resources);
    		this.treeControl = loader.getController(); 
    		this.treeControl.setDbManager(this.dbManager);
    		
    		treeControl.setParentControl(this);
    		topLeft.getChildren().add(treeView);
       	} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			LOG.debug("Cannot read properties file");
			e.printStackTrace();
		}
    }
    
    /**
     * sets up the email table
     */
	private void initTopRight() {
		try {
    		FXMLLoader loader = new FXMLLoader();
            loader.setResources(resources);
            loader.setLocation(getClass().getResource("/fxml/tableLayout.fxml"));
    		AnchorPane tableView = loader.load();
            loader.setResources(resources);
    		tableControl = loader.getController(); 
    		tableControl.setDbManager(this.dbManager);
    		
    		tableControl.setParentControl(this);
    		topRight.getChildren().add(tableView);   		
    	} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			LOG.debug("Cannot read properties file");
			e.printStackTrace();
		}
	}
	
	/**
	 * sets up the web viewer
	 */
	public void initBottomLeft() {
		try {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(resources);
            loader.setLocation(getClass().getResource("/fxml/webLayout.fxml"));
            AnchorPane webView = (AnchorPane) loader.load();
            loader.setResources(resources);
            webControl = loader.getController();
            webControl.setDbManager(this.dbManager);
    		if(!bottomLeft.getChildren().isEmpty()) {
    			bottomLeft.getChildren().remove(0);
    		}
            bottomLeft.getChildren().add(webView);
        } catch (IOException ex) {
            LOG.error("initUpperRightLayout error", ex);
            LOG.error("initUpperRightLayout()");
            Platform.exit();
        }
	}
	
	/**
	 * sets up the html editor
	 */
	private void initBottomRight() {
		try {
    		FXMLLoader loader = new FXMLLoader();
            loader.setResources(resources);
            loader.setLocation(getClass().getResource("/fxml/editLayout.fxml"));
    		AnchorPane editView = loader.load();
    		htmlControl = loader.getController();
    		htmlControl.setDbManager(this.dbManager);
    		htmlControl.setPropFile(this.prop);
    		
    		htmlControl.setParentControl(this);
    		bottomRight.getChildren().add(editView);
    	} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			LOG.debug("Cannot read properties file");
			e.printStackTrace();
		}
	}

	/**
	 * property to the main method
	 */
	public void setRoot(Jag jag) {
		this.jag = jag;
	}
}
