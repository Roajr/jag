package presentation;

import java.io.File;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import business.MailController;
import data.LoginBeanFx;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.stage.Window;
import jodd.mail.RFC2822AddressParser;
import jodd.mail.SendMailSession;
import jodd.mail.SmtpServer;


public class LoginController {
	
    private final static Logger LOG = LoggerFactory.getLogger(MailController.class);
    public boolean loggedIn = false;
    private LoginBeanFx loginBeanFx;
    MailController mailControl = new MailController();
    
    @FXML
    private ResourceBundle resources;
    
    @FXML
    private GridPane Login;
    
    @FXML
    private TextField dbuser;

    @FXML
    private TextField dbpwd;
    
    @FXML
    private TextField db;

    @FXML
    private TextField emailAddress;

    @FXML
    private TextField emailPwd;

    @FXML
    private Button login;

    @FXML
    private Button exit;
    
    @FXML
    private void initialize() {
        Bindings.bindBidirectional(emailAddress.textProperty(), loginBeanFx.emailAddProperty());
        Bindings.bindBidirectional(emailPwd.textProperty(), loginBeanFx.emailPwdProperty());
        Bindings.bindBidirectional(dbuser.textProperty(), loginBeanFx.dbUserProperty());
        Bindings.bindBidirectional(dbpwd.textProperty(), loginBeanFx.dbPwdProperty());
        Bindings.bindBidirectional(db.textProperty(), loginBeanFx.dbProperty());
    }
    
    /**
     * exits the progra,
     */
	@FXML
    protected void handleExit(){
        System.exit(0);
    }
    
    /**
     * Handles the login click
     * 
     * @param event client pressed login
     * @throws SQLException
     * @throws IOException
     */
    @FXML
    protected void handleLogin(ActionEvent event) throws SQLException, IOException{
        Properties prop = new Properties(); 
        if(validateData()) {
        	prop.setProperty("USERT", dbuser.getText());
            prop.setProperty("PASSWORDT", dbpwd.getText());
            prop.setProperty("sendEmailT", emailAddress.getText());
            prop.setProperty("sendT", emailPwd.getText());
            LOG.debug("Valid Login");
          //Stores all the data with validated info
            prop.setProperty("sendEmail", loginBeanFx.getEmailAdd());
            prop.setProperty("receiveEmail", "receive.1439367@gmail.com");
            prop.setProperty("otherEmail", "other.1439367@gmail.com");
            prop.setProperty("send", loginBeanFx.getEmailPwd());
            prop.setProperty("receive", "DawReceive143");
            prop.setProperty("other", "DawOther143");
            prop.setProperty("URL", "jdbc:mysql://localhost:3306/" + loginBeanFx.getDb() +  "?autoReconnect=true&useSSL=false&allowPublicKeyRetrieval=true");
            prop.setProperty("USER", loginBeanFx.getDbUser());
            prop.setProperty("PASSWORD", loginBeanFx.getDbPwd());
            File file = new File("jag.properties");
            FileOutputStream fileOut = new FileOutputStream(file);
            prop.store(fileOut, "Favorite Things");
            fileOut.close();
            Window scene = Login.getScene().getWindow();
            scene.hide();
            loadMainApp();
        }else {
        	LOG.debug("Not Valid Login");
        }
    }

    /**
     * creates the login bean
     */
    public LoginController(){
        loginBeanFx = new LoginBeanFx();
    }
    
    /**
     * gets the loginBean 
     * @return login Bean
     */
    public LoginBeanFx getLoginBeanFx(){
        return loginBeanFx;
    }
    
    /**
     * Validates the fields of the login scene to see if client can login
     * 
     * @return boolen of if data is valid
     */
    private boolean validateData() {
    	try(Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/" + loginBeanFx.getDb() +  "?autoReconnect=true&useSSL=false&allowPublicKeyRetrieval=true" ,loginBeanFx.getDbUser(),loginBeanFx.getDbPwd())){
    		SmtpServer server = mailControl.getSmtpServer(RFC2822AddressParser.STRICT.parseToEmailAddress(loginBeanFx.getEmailAdd()).toString(), loginBeanFx.getEmailPwd());
    		SendMailSession session = server.createSession();
    		try {
    			session.open();
    			session.close();
    			return true;
    		}catch(Exception e) {
    			return false;
    		}
    	}catch (Exception e){
        	return false;
    	}
    }
        
    /**
     * Loads the main app if you needed to login
     */
    private void loadMainApp() {
    	try {
    		Stage window = new Stage(); 
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/rootLayout.fxml"));
            Parent root = loader.load();
            RootController controller = loader.getController();
            
            Scene scene = new Scene(root);
            window.setTitle("JAG");
            window.setScene(scene);
            window.show();
        } catch (IOException | IllegalStateException ex) {
            LOG.error("Unable to load fxml", ex);
            LOG.error(ex.getMessage());
        }		
	}
}
