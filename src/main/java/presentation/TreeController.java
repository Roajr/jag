package presentation;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.scene.control.TreeView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;

import java.sql.SQLException;


import business.DatabaseController;
import business.MailController;
import data.EmailBean;
import data.FolderBeanFx;


public class TreeController {

    private final static Logger LOG = LoggerFactory.getLogger(MailController.class);
    private DatabaseController dbManager;
    private FolderBeanFx rootFolder = new FolderBeanFx();
    private RootController rootControl;
    private TreeItem<FolderBeanFx> rootItem;
    private ContextMenu contextMenu;
    
    @FXML
    private ResourceBundle resources;

    @FXML
    private Button showBtn;
    
    @FXML
    private TreeView<FolderBeanFx> treeView;

    @FXML
    private TextField folderName;
    
    @FXML
    private Button Add;

    @FXML
    void initialize() throws SQLException {    	
    	FolderBeanFx rootFolder = new FolderBeanFx();
    	
    	rootFolder.setFolderName("Folders");
    	rootFolder.setId(-1);
    	
    	rootItem = new TreeItem<>(rootFolder);
    	treeView.setRoot(rootItem);
    	
    	treeView.setCellFactory((e) -> new TreeCell<FolderBeanFx>() {
            @Override
            protected void updateItem(FolderBeanFx item, boolean empty) {
                super.updateItem(item, empty);
                if (item != null) {
                    setText(item.getFolderName());
                } else {
                    setText("");
                }
            }
        });
    	contextMenu = new ContextMenu();
    }
    
    /**
     * drag over to accecpt email data
     * @param event
     */
    @FXML
    private void dragOver(DragEvent event) {
    	/* data is dragged over the target */
        LOG.debug("onDragOver");

        // Accept it only if it is not dragged from the same control and if it
        // has a string data
        if (event.getGestureSource() != treeView && event.getDragboard().hasString()) {
            // allow for both copying and moving, whatever user chooses
            event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
        }
        event.consume();
    }
    
    /**
     * to add email to a folder
     * @param event
     * @throws NumberFormatException
     * @throws SQLException
     */
    @FXML
    private void dragDrop(DragEvent event) throws NumberFormatException, SQLException {
        Dragboard db = event.getDragboard();
    	dbManager.changeFolder(Integer.parseInt(db.getString()), event.getTarget().toString().split("\"")[1]);
    	this.rootControl.tableControl.displayTheTable(dbManager.getFolderId(event.getTarget().toString().split("\"")[1]));
    }
    
    /**
     * handles if the client click a folder on the folder tree
     * 
     * @param event
     * @throws SQLException
     */
    @FXML
    public void handleClick(MouseEvent event) throws SQLException {
        FolderBeanFx ev = treeView.getSelectionModel().getSelectedItem().getValue();        	
        this.rootControl.tableControl.displayTheTable(ev.getId());
    }
    
    /**
     * method to remove a folder
     * @param event
     * @throws SQLException
     */
    @FXML
    private void rightClick(ContextMenuEvent event) throws SQLException {
    	FolderBeanFx ev = treeView.getSelectionModel().getSelectedItem().getValue();
    	if(ev.getFolderName().equals("inbox") || ev.getFolderName().equals("sent")) {
    		Alert alert = new Alert(AlertType.INFORMATION);
    		alert.setTitle(resources.getString("cantFolder"));
    		alert.setHeaderText(resources.getString("cantFolder") + ev.getFolderName());
    		alert.setContentText(ev.getFolderName() + resources.getString("rootFolder"));
    	}else {
        	Alert alert = new Alert(AlertType.CONFIRMATION);
        	alert.setTitle(resources.getString("delCon"));
        	alert.setHeaderText(resources.getString("delFol")+ ev.getFolderName());
        	alert.setContentText(resources.getString("sure"));
        	Optional<ButtonType> result = alert.showAndWait();
        	if(result.get() == ButtonType.OK) {
        		List<EmailBean> list = this.dbManager.findAll(ev.getId());
        		for(EmailBean email : list) {
        			this.dbManager.changeFolder(email.getEmailID(), "inbox");
        		}
        		this.dbManager.removeFolder(ev.getId());
        		
        		this.rootControl.tableControl.displayTheTable();
        		displayTree();
        	}	
    	}
    }
    
    /**
     * method to add a folder
     * @throws SQLException
     */
    @FXML
    private void addFolder() throws SQLException {
    	this.dbManager.makeNewFolder(folderName.getText());
    	displayTree();
    }
    
    /**
     * gets the root to be able to reload the table
     * 
     * @param rootControl
     */
    public void setParentControl(RootController rootControl) {
    	this.rootControl = rootControl;
    }

    /**
     * displays the tree with all folder data
     * @throws SQLException
     */
    public void displayTree() throws SQLException {
    	this.treeView.getRoot().getChildren().clear();
    	this.rootControl.tableControl.displayTheTable();
    	ObservableList<FolderBeanFx> folders;
		try {
			folders = dbManager.getFolders();
			if (folders != null) {
	        	folders.stream().map((fd) -> new TreeItem<>(fd)).map((item) -> {
	                return item;
	            }).forEachOrdered((item) -> {
	            	treeView.getRoot().getChildren().add(item);
	            });
	        }
			this.treeView.getRoot().setExpanded(true);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    /**
	 * Sets up the dbManager off the properties file
	 */
    public void setDbManager(DatabaseController dbManager) {
    	this.dbManager = dbManager;
    }
}
