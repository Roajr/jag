package presentation;
                  
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import business.DatabaseController;
import business.MailController;
import data.AttachmentObj;
import data.EmailBean;
import data.EmailBeanFx;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.web.HTMLEditor;
import javafx.stage.FileChooser;
import jodd.mail.RFC2822AddressParser;
import threads.SendEmailThread;

public class HtmlController {
    private final static Logger LOG = LoggerFactory.getLogger(MailController.class);
    private DatabaseController dbManager;
    private RootController rootControl;
    private MailController mailControl;
    private Properties prop;
    private EmailBeanFx emailBeanFx;
    private List<String> toEmails = new ArrayList<>();
    private List<String> ccEmails = new ArrayList<>();
    private List<String> bccEmails = new ArrayList<>();
    private List<AttachmentObj> attachments = new ArrayList<>();
    
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private AnchorPane editTable;

    @FXML
    private BorderPane editViewer;

    @FXML
    private MenuBar menu;

    @FXML
    private Button sendBtn;

    @FXML
    private TextField toField;

    @FXML
    private TextField ccField;

    @FXML
    private TextField bccField;

    @FXML
    private TextField subjectField;

    @FXML
    private HTMLEditor htmlField;

    @FXML
    private Button toAdd;

    @FXML
    private Button ccAdd;

    @FXML
    private Button bccAdd;
    
    @FXML
    private Button attachBtn;
    
    @FXML
    private Label att;
    
    @FXML
    void initialize() throws SQLException {
    }

    /**
     * will send a email with the data in the html editor
     */
    @FXML
    void handleSend() throws SQLException, IOException{
    	String[] emails = toField.getText().split(",");
    	for(String email : emails) {
    		if(!toEmails.contains(email)) {
    			toEmails.add(RFC2822AddressParser.STRICT.parseToEmailAddress(email).toString());
    		}
    	}
    	emails = ccField.getText().split(",");
    	for(String email : emails) {
    		if(!ccEmails.contains(email) && !ccEmails.isEmpty()) {
    			ccEmails.add(RFC2822AddressParser.STRICT.parseToEmailAddress(email).toString());
    		}
    	}
    	emails = bccField.getText().split(",");
    	for(String email : emails) {
    		if(!bccEmails.contains(email) && !bccEmails.isEmpty()) {
    			bccEmails.add(RFC2822AddressParser.STRICT.parseToEmailAddress(email).toString());
    		}
    	}
    	makeEmail();
    }
    
    /**
     * attaches the email to the email bean 
     * @throws IOException
     */
    @FXML
    void attachImg() throws IOException {
    	Alert alert = new Alert(AlertType.CONFIRMATION);
    	alert.setTitle(resources.getString("attachIns"));
    	alert.setHeaderText(resources.getString("insMsg"));
    	
    	ButtonType buttonTypeEmb = new ButtonType(resources.getString("emb"));
    	ButtonType buttonTypeAtc = new ButtonType(resources.getString("atc"));
    	alert.getButtonTypes().setAll(buttonTypeEmb,buttonTypeAtc);
    	Optional<ButtonType> result = alert.showAndWait();
    	
    	if(result.get() == buttonTypeAtc) {
    		FileChooser fileChooser = new FileChooser();
        	File selectedFile = fileChooser.showOpenDialog(null);
        	if (selectedFile != null) {
        		AttachmentObj file = new AttachmentObj(selectedFile.getName(),selectedFile.getAbsolutePath());
        		attachments.add(file);
        	}else {
        		LOG.debug("File selection cancelled.");
        	}	
    	}else if(result.get() == buttonTypeEmb) {
    		FileChooser fileChooser = new FileChooser();
        	File selectedFile = fileChooser.showOpenDialog(null);
        	if (selectedFile != null) {
        		String text = htmlField.getHtmlText();
        		htmlField.setHtmlText(text + "<br><span><img src='cid:"+selectedFile.getName()+"' alt='" + selectedFile.getName() + "'/></span>");
        	}else {
        		LOG.debug("File selection cancelled.");
        	}
    	}
    	att.setText("");
    	for(AttachmentObj attachment : attachments) {
    		String t = att.getText();
    		att.setText(t+" "+attachment.getName());
    	}
    }

    /**
     * adds a email to the correct list
     * @param event
     */
	@FXML
    void toAdd(ActionEvent event) {
    	int id = event.getTarget().toString().indexOf("id=");
    	if(event.getSource().toString().substring(id+3, id+4).equals("t")) {
    		String inputData = inputEmailDialog();
        	if(!inputData.equals("null")) {
            	toEmails.add(RFC2822AddressParser.STRICT.parseToEmailAddress(inputData).toString());
            	toField.setText("");
            	for(String emails : toEmails) {
            		if(toField.getText().equals("")) {
            			toField.setText(emails);
            		}else {
                		toField.setText(toField.getText() + " , " + emails);
            		}
            	}
        	}
    	}else if(event.getSource().toString().substring(id+3, id+4).equals("c")) {
    		String inputData = inputEmailDialog();
        	if(!inputData.equals("null")) {
        		ccEmails.add(RFC2822AddressParser.STRICT.parseToEmailAddress(inputData).toString());
        		ccField.setText("");
            	for(String emails : ccEmails) {
            		if(ccField.getText().equals("")) {
            			ccField.setText(emails);
            		}else {
            			ccField.setText(ccField.getText() + " , " + emails);
            		}
            	}
        	}
    	}else if(event.getSource().toString().substring(id+3, id+4).equals("b")) {
    		String inputData = inputEmailDialog();
        	if(!inputData.equals("null")) {
        		bccEmails.add(RFC2822AddressParser.STRICT.parseToEmailAddress(inputData).toString());
        		bccField.setText("");
            	for(String emails : bccEmails) {
            		if(bccField.getText().equals("")) {
            			bccField.setText(emails);
            		}else {
            			bccField.setText(bccField.getText() + " , " + emails);
            		}
            	}
        	}
    	}
    }
    
	/**
	 * removes a email from the correct list
	 * @param event
	 */
    @FXML 
    void toRemove(ActionEvent event) {
    	int id = event.getTarget().toString().indexOf("id=remove");
    	if(event.getSource().toString().substring(id+9, id+10).equals("T")) {
    		ChoiceDialog<String> dialog = new ChoiceDialog<>("select a email", toEmails);
        	Optional<String> result = dialog.showAndWait();
        	if (result.isPresent()) {
        		toEmails.remove(result.get());
            	toField.setText("");
        		for(String emails : toEmails) {
            		if(toField.getText().equals("")) {
            			toField.setText(emails);
            		}else {
                		toField.setText(toField.getText() + " , " + emails);
            		}
            	}
        	}
    	}else if(event.getSource().toString().substring(id+9, id+10).equals("C")) {
    		ChoiceDialog<String> dialog = new ChoiceDialog<>("select a email", ccEmails);
        	Optional<String> result = dialog.showAndWait();
        	if (result.isPresent()) {
        		ccEmails.remove(result.get());
            	ccField.setText("");
        		for(String emails : ccEmails) {
            		if(ccField.getText().equals("")) {
            			ccField.setText(emails);
            		}else {
            			ccField.setText(ccField.getText() + " , " + emails);
            		}
            	}
        	}
    	}else if(event.getSource().toString().substring(id+9, id+10).equals("B")) {
    		ChoiceDialog<String> dialog = new ChoiceDialog<>("select a email", bccEmails);
        	Optional<String> result = dialog.showAndWait();
        	if (result.isPresent()) {
        		bccEmails.remove(result.get());
        		bccField.setText("");
        		for(String emails : bccEmails) {
            		if(bccField.getText().equals("")) {
            			bccField.setText(emails);
            		}else {
            			bccField.setText(bccField.getText() + " , " + emails);
            		}
            	}
        	}
    	}
    }
    
    /**
     * makes a email and returns a boolean if it passes
     * @return
     * @throws IOException
     */
    public boolean makeEmail() throws IOException {    	
    	String editorText = htmlField.getHtmlText();
		EmailBean email = new EmailBean();
		email.setEmailID(-1);
		email.setFromEmail(prop.getProperty("sendEmail"));
		email.setToEmail(toEmails);
		email.setCcEmail(ccEmails);
		email.setBccEmail(bccEmails);
		email.setSubject(subjectField.getText());
		List<String> text = new ArrayList();
		text.add("UTF-8");
		text.add("TEXT/PLAIN");
		text.add(editorText);
		email.setTextMessage(text);
		List<String> html = new ArrayList();
		html.add("UTF-8");
		html.add("TEXT/HTML");
		html.add(editorText);
		email.setHtmlMessage(html);
		email.setAttachment(attachments);
		email.setTypeOfMessage("sent"); 
		email.setLink(1);
		email.setSendTime(LocalDateTime.now().withNano(0).toString());
		email.setReceiveTime("");
		email.setPriorty(5);
		email.setAttachment(attachments);
		SendEmailThread sendThread = new SendEmailThread(this.dbManager, this.prop, email);
		sendThread.start();
		refresh();
		return true;
    }
    
    /**
     * refreshes the html editor 
     */
    private void refresh() {
    	toEmails = new ArrayList<>();
    	ccEmails = new ArrayList<>();
        bccEmails = new ArrayList<>();
        attachments = new ArrayList<>();
        toField.setText("");
        ccField.setText("");
        bccField.setText("");
        subjectField.setText("");
        htmlField.setHtmlText("");
	}
        
    /**
     * default controller
     */
    public HtmlController() throws SQLException{
    	this.mailControl = new MailController();
    	emailBeanFx =  new EmailBeanFx();
    }
    
    /**
     * gets the emailbean from the the htmlEditor
     * @return emailBean
     */
    public EmailBeanFx getEmailBeanFx() {
    	return emailBeanFx;
    }
    
    /**
     * dialog to add a email
     * @return
     */
    private String inputEmailDialog() {
    	TextInputDialog dialog = new TextInputDialog("");
    	dialog.setTitle(resources.getString("emailInp"));
    	dialog.setHeaderText(resources.getString("emailInph"));
    	dialog.setContentText(resources.getString("EmailAdd"));
    	Optional<String> result = dialog.showAndWait();
    	if (result.isPresent()) {
    		return result.get();
    	}
    	else return null;
    }
   
	/**
	 * Sets up the dbManager off the properties file
	 */
    public void setDbManager(DatabaseController dbManager) {
    	this.dbManager = dbManager;
    }
    
    /**
     * sets the prop file
     * @param prop
     */
    public void setPropFile(Properties prop) {
    	this.prop = prop;
    }

    /**
     * sets the db manager
     * @param rootController
     */
	public void setParentControl(RootController rootController) {
    	this.rootControl = rootControl;		
	}

}
