package presentation;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import business.DatabaseController;
import business.MailController;
import data.EmailBean;
import data.EmailBeanFx;
import data.WebBeanFx;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;

public class TableController {

    private final static Logger LOG = LoggerFactory.getLogger(MailController.class);
    private DatabaseController dbManager;
    private RootController rootControl;
    private EmailBean dragEmail;
    
    @FXML
    private ResourceBundle resources;
    
    @FXML
    private AnchorPane emailTable;

    @FXML
    private TableView<EmailBeanFx> tableView;

    @FXML
    private TableColumn<EmailBeanFx, Number> id;

    @FXML
    private TableColumn<EmailBeanFx, String> from;

    @FXML
    private TableColumn<EmailBeanFx, String> to;

    @FXML
    private TableColumn<EmailBeanFx, String> cc;

    @FXML
    private TableColumn<EmailBeanFx, String> bcc;

    @FXML
    private TableColumn<EmailBeanFx, String> subject;

    @FXML
    private TableColumn<EmailBeanFx, String> message;

    @FXML
    private TableColumn<EmailBeanFx, String> attachment;

    @FXML
    private TableColumn<EmailBeanFx, String> typeOfMessage;

    @FXML
    private TableColumn<EmailBeanFx, String> folder;

    @FXML
    private TableColumn<EmailBeanFx, String> sendtime;

    @FXML
    private TableColumn<EmailBeanFx, String> receivetime;
    
    @FXML
    void initialize() {
    	id.setCellValueFactory(cellData -> cellData.getValue()
                .getEmailIDFxProperty());
    	from.setCellValueFactory(cellData -> cellData.getValue()
                .getFromEmailFxProperty());
    	to.setCellValueFactory(cellData -> cellData.getValue()
                .getToEmailFxProperty());
    	cc.setCellValueFactory(cellData -> cellData.getValue()
                .getCcEmailFxProperty());
    	bcc.setCellValueFactory(cellData -> cellData.getValue()
                .getBccEmailFxProperty());
    	subject.setCellValueFactory(cellData -> cellData.getValue()
                .getsubjectFxProperty());
    	attachment.setCellValueFactory(cellData -> cellData.getValue()
                .getAttachmentFxProperty());
    	typeOfMessage.setCellValueFactory(cellData -> cellData.getValue()
                .getTypeOfMessageFxProperty());
    	folder.setCellValueFactory(cellData -> cellData.getValue()
                .getLinkFxProperty());
    	sendtime.setCellValueFactory(cellData -> cellData.getValue()
                .getSendTimeFxProperty());
    	receivetime.setCellValueFactory(cellData -> cellData.getValue()
                .getReceiveTimeFxProperty());
    	adjustColumnWidths();
    }
    
    /**
     * get the id of the email you started dragging
     * 
     * @param event
     * @throws SQLException
     */
    @FXML
    public void handleDrag(MouseEvent event) throws SQLException {
    	EmailBeanFx ev = tableView.getSelectionModel().getSelectedItem();    	
    	Dragboard db = tableView.startDragAndDrop(TransferMode.ANY);
    	ClipboardContent contents = new ClipboardContent();
    	contents.putString(String.valueOf(ev.getEmailIDFx()));
    	db.setContent(contents);
    	event.consume();
    }
    
    /**
     * 
     * @param event
     * @throws SQLException
     * @throws FileNotFoundException 
     * @throws IOException
     */
    @FXML
    public void handleClick(MouseEvent event) throws SQLException, FileNotFoundException {
    	EmailBeanFx ev = tableView.getSelectionModel().getSelectedItem();
    	
    	EmailBean email = dbManager.findID(ev.getEmailIDFx());
    	WebBeanFx webView = new WebBeanFx(email.getHtmlMessage().get(2), email.getSubject());
    	
    	File htmlFile = new File("example.html");
    	if(htmlFile.exists()) {
    		FileOutputStream file = new FileOutputStream(htmlFile);
    		PrintStream print = new PrintStream(file);
    		print.print(webView.getBody());
    		print.close();
    	}
    	this.rootControl.initBottomLeft();
    	this.rootControl.webControl.reloadWeb(email);

    }
    
    /**
     * right click to remove a email
     * @param event
     * @throws SQLException
     */
    @FXML
    public void rightClick(ContextMenuEvent event) throws SQLException {
    	EmailBeanFx ev = tableView.getSelectionModel().getSelectedItem();
    	Alert alert = new Alert(AlertType.CONFIRMATION);
    	alert.setTitle(resources.getString("delCon"));
    	alert.setHeaderText(resources.getString("delQue") + ev.getEmailIDFx() + resources.getString("id"));
    	alert.setContentText(resources.getString("sure"));
    	Optional<ButtonType> result = alert.showAndWait();
    	if(result.get() == ButtonType.OK) {
    		this.dbManager.delete(ev.getEmailIDFx());
    		displayTheTable();
    	}	
    }
    
    /**
     * gets parent root to be able to reload the web view 
     * 
     * @param rootControl
     */
    public void setParentControl(RootController rootControl) {
    	this.rootControl = rootControl;
    }
    
    /**
     * sets all columns of the table to the same size
     */
    private void adjustColumnWidths() {
        // Get the current width of the table
        double width = emailTable.getPrefWidth();
        // Set width of each column
        id.setPrefWidth(width * .05);
        from.setPrefWidth(width * .1);
        to.setPrefWidth(width*.1);
        cc.setPrefWidth(width*.1);
        bcc.setPrefWidth(width*.1);
        subject.setPrefWidth(width*.1);
        attachment.setPrefWidth(width*.1);
        typeOfMessage.setPrefWidth(width*.1);
        folder.setPrefWidth(width*.1);
        sendtime.setPrefWidth(width*.1);
        receivetime.setPrefWidth(width*.1);

    }
    
    /**
     * fills the table with all emails
     * 
     * @throws SQLException
     */
    public void displayTheTable() throws SQLException {
    	tableView.setItems(this.dbManager.FindTableAll());
    }
    
    /**
     * displays the table with values that are in a folder
     * 
     * @param link folder id
     * @throws SQLException
     */
    public void displayTheTable(int link) throws SQLException {
    	tableView.setItems(this.dbManager.FindTableAll(link));
    }
    
    /**
     * gets the emailBean of the table
     * 
     * @return
     */
    public TableView<EmailBeanFx> getemailTable(){
    	return tableView;
    }
    
    /**
	 * Sets up the dbManager off the properties file
	 */
    public void setDbManager(DatabaseController dbManager) {
    	this.dbManager = dbManager;
    }
}
