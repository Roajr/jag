package logic;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import business.MailController;
import data.EmailBean;
import jodd.mail.Email;
import jodd.mail.EmailAttachment;
/**
 * Class to create email object
 * 
 * @author Jamroa
 *
 */
public class EmailCreate {
    private final static Logger LOG = LoggerFactory.getLogger(MailController.class);

    private EmailBean email;
	private String htmlIntro = "<html><META http-equiv=Content-Type content=\"text/html; charset=utf-8\"><body>";
	private String htmlEnd = "</body></html>";

	/**
	 * Creates a emailBoject from the information that is in the emailBean
	 * 
	 * @param emailInfo
	 * @return email
	 */
	public Email makeEmail(EmailBean emailInfo) {		
		this.email = emailInfo;
		
		Email emailObject = Email.create().from(this.email.getFromEmail());
				for(int toCount = 0; toCount < this.email.getToEmail().size(); toCount++) {
					emailObject.to(this.email.getToEmail().get(toCount).toString());
				}
				for(int ccCount = 0; ccCount < this.email.getCcEmail().size(); ccCount++) {
					emailObject.cc(this.email.getCcEmail().get(ccCount).toString());
				}
				for(int bccCount = 0; bccCount < this.email.getBccEmail().size(); bccCount++) {
					emailObject.bcc(this.email.getBccEmail().get(bccCount).toString());
				}
				emailObject.subject(this.email.getSubject())
				.textMessage(this.email.getTextMessage().get(2).toString())
				.htmlMessage(htmlIntro + this.email.getHtmlMessage().get(2).toString() + htmlEnd);
				for(int attachmentCount = 0; attachmentCount < this.email.getAttachment().size(); attachmentCount++) {
					if(this.email.getHtmlMessage().get(2).toString().contains(this.email.getAttachment().get(attachmentCount).getName().concat(this.email.getAttachment().get(attachmentCount).getType()))) {
						emailObject.embeddedAttachment(EmailAttachment.with().name(this.email.getAttachment().get(attachmentCount).getName().concat(".".concat(this.email.getAttachment().get(attachmentCount).getType())))
								.content(this.email.getAttachment().get(attachmentCount).getFile()));
					}else {
						emailObject.attachment(EmailAttachment.with().name(this.email.getAttachment().get(attachmentCount).getName().concat(".".concat(this.email.getAttachment().get(attachmentCount).getType())))
								.content(this.email.getAttachment().get(attachmentCount).getFile()));
					}
				}
				emailObject.priority(this.email.getPriorty());
				
    	return emailObject;
	} 
}