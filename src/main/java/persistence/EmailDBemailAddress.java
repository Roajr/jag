package persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import business.MailController;
import data.EmailBean;

public class EmailDBemailAddress {
    
	private final static Logger LOG = LoggerFactory.getLogger(MailController.class);
	Connection conn;
	PreparedStatement updateEmailAddress = null;
	String emailAddressInsert = "Insert into emailaddresstable"
			+ "(emailAddressID, emailAddress) values"
			+ "(?,?)";
	PreparedStatement findEmailAddress = null;
	String findEmailSelect = "select emailAddressID from emailaddresstable where emailAddress = ?"; 
	PreparedStatement getFromEmail = null;
	String fromEmailSelect = "SELECT * from emailaddresstable join emailaddressbridge on emailaddresstable.emailAddressID=emailaddressbridge.addressKey where emailaddressbridge.emailKey = ? and type = 'fr'";
	PreparedStatement getToEmail = null;
	String toEmailSelect = "SELECT * from emailaddresstable join emailaddressbridge on emailaddresstable.emailAddressID=emailaddressbridge.addressKey where emailaddressbridge.emailKey = ? and type = 'to'";
	PreparedStatement getCcEmail = null;
	String ccEmailSelect = "SELECT * from emailaddresstable join emailaddressbridge on emailaddresstable.emailAddressID=emailaddressbridge.addressKey where emailaddressbridge.emailKey = ? and type = 'cc'";
	PreparedStatement getBcEmail = null;
	String bcEmailSelect = "SELECT * from emailaddresstable join emailaddressbridge on emailaddresstable.emailAddressID=emailaddressbridge.addressKey where emailaddressbridge.emailKey = ? and type = 'bc'";

	/**
	 * Will update the bridge table and the address table a email addres if it is not already there
	 * 
	 * @param dbaddressbridge
	 * @param toEmail
	 * @param addressID
	 * @param emailID
	 * @param type
	 * @param conn
	 * @return
	 * @throws SQLException
	 */
	public boolean insertIntoEmailAddressTable(EmailDBemailBridge dbaddressbridge, String toEmail, int addressID, int emailID, String type, Connection conn) throws SQLException {
		this.conn = conn;
		findEmailAddress = conn.prepareStatement(findEmailSelect);
   		findEmailAddress.setString(1, toEmail);
   		ResultSet rs = findEmailAddress.executeQuery();
   		if(rs.next() == false) {	
   			updateEmailAddress = conn.prepareStatement(emailAddressInsert);
   			updateEmailAddress.setInt(1, addressID);
   			updateEmailAddress.setString(2, toEmail);
   			updateEmailAddress.executeUpdate();
   			
   			dbaddressbridge.insertIntoEmailAddressTableBridge(emailID, addressID, type, conn);
   			return true;
   		}else {
   			dbaddressbridge.insertIntoEmailAddressTableBridge(emailID, rs.getInt(1), type, conn);
   			return false;
   		}
	}
	
	/**
	 * gets the from email id and returns the bean with the from email filled out
	 * @param emailInfo
	 * @param emailID
	 * @param conn
	 * @return
	 * @throws SQLException
	 */
	public EmailBean getFromEmail(EmailBean emailInfo, int emailID, Connection conn) throws SQLException {
		this.conn = conn;
		getFromEmail = conn.prepareStatement(fromEmailSelect);
		getFromEmail.setInt(1, emailID);
		ResultSet rs = getFromEmail.executeQuery();
		while(rs.next()) {
			emailInfo.setFromEmail(rs.getString(2));
		}
		return emailInfo;
	}
	
	/**
	 * Retrieves all the to emails for a emailID
	 * 
	 * @param emailInfo
	 * @param emailID
	 * @param conn
	 * @return the passed emailBean with the to filled in
	 * @throws SQLException
	 */
	public EmailBean getToEmail(EmailBean emailInfo, int emailID, Connection conn) throws SQLException {
		this.conn = conn;
		getToEmail = conn.prepareStatement(toEmailSelect);
		getToEmail.setInt(1, emailID);
		ResultSet rs = getToEmail.executeQuery();
		List<String> toEmails = new ArrayList<String>();
		while(rs.next()) {
			toEmails.add(rs.getString(2));
		}
		emailInfo.setToEmail(toEmails);
   		return emailInfo;
	}

	/**
	 * Retrieves all the cc emails for a emailID
	 * 
	 * @param emailInfo
	 * @param emailID
	 * @param conn
	 * @return the passed emailBean with the cc filled in
	 * @throws SQLException
	 */
	public EmailBean getCcEmail(EmailBean emailInfo, int emailID, Connection conn) throws SQLException {
		this.conn = conn;
		getCcEmail = conn.prepareStatement(ccEmailSelect);
		getCcEmail.setInt(1, emailID);
		ResultSet rs = getCcEmail.executeQuery();
		List<String> ccEmails = new ArrayList<String>();
		while(rs.next()) {
			ccEmails.add(rs.getString(2));
		}
		emailInfo.setCcEmail(ccEmails);
   		return emailInfo;
	}

	/**
	 * Retrieves all the to emails for a emailID
	 * 
	 * @param emailInfo
	 * @param emailID
	 * @param conn
	 * @return the passed emailBean with the bcc filled in
	 * @throws SQLException
	 */
	public EmailBean getBcEmail(EmailBean emailInfo, int emailID, Connection conn) throws SQLException {
		this.conn = conn;
		getBcEmail = conn.prepareStatement(bcEmailSelect);
		getBcEmail.setInt(1, emailID);
		ResultSet rs = getBcEmail.executeQuery();
		List<String> bcEmails = new ArrayList<String>();
		while(rs.next()) {
			bcEmails.add(rs.getString(2));
		}
		emailInfo.setBccEmail(bcEmails);
   		return emailInfo;
	}
	
}
