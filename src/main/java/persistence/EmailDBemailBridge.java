package persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class EmailDBemailBridge {

	Connection conn;
	PreparedStatement updateEmailAddressBridge = null;
	String emailAddressBridgeInsert = "Insert into emailaddressbridge"
			+ "(emailKey, addressKey, type) values"
			+ "(?,?,?)";

	/**
	 * inserts into the email bridge
	 * 
	 * @param emailID
	 * @param addressID
	 * @param type
	 * @param conn
	 * @throws SQLException
	 */
	void insertIntoEmailAddressTableBridge(int emailID, int addressID, String type, Connection conn) throws SQLException {
		this.conn = conn;
		updateEmailAddressBridge = conn.prepareStatement(emailAddressBridgeInsert);
		updateEmailAddressBridge.setInt(1, emailID);
		updateEmailAddressBridge.setInt(2, addressID);
		updateEmailAddressBridge.setString(3, type);	
		updateEmailAddressBridge.executeUpdate();
	}
}
