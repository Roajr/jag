package persistence;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import business.MailController;
import data.AttachmentObj;
import data.EmailBean;

public class EmailDBattachments {
	
    private final static Logger LOG = LoggerFactory.getLogger(MailController.class);
	Connection conn;
	PreparedStatement updateEmailAttachments = null;
	String emailAttachmentsInsert = "Insert into attachmentstable"
			+ "(attachmentEmail, attachmentName, attachmentType, attachmentData) values"
			+ "(?,?,?,?)";
	PreparedStatement getEmailAttachments = null;
	String emailAttachmentsGet = "SELECT * FROM attachmentstable where attachmentEmail = ?";
	
	/**
	 * insets a attachment into the attachment table
	 * 
	 * @param attachment
	 * @param emailID
	 * @param conn
	 * @throws SQLException
	 */
	public void insertIntoAttachmentsTable(AttachmentObj attachment, int emailID, Connection conn) throws SQLException {
		this.conn = conn;
   		updateEmailAttachments = conn.prepareStatement(emailAttachmentsInsert);
       	updateEmailAttachments.setInt(1, emailID);
       	updateEmailAttachments.setString(2, attachment.getName());
       	updateEmailAttachments.setString(3, attachment.getType());
        updateEmailAttachments.setBytes(4, attachment.getFile());
       	updateEmailAttachments.executeUpdate();
	}
	
	/**
	 * Retrieves a attachment from the attachment table given a id
	 * 
	 * @param emailInfo
	 * @param emailID
	 * @param conn
	 * @return
	 * @throws SQLException
	 */
	public EmailBean getFromAttachmentsTable(EmailBean emailInfo, int emailID, Connection conn) throws SQLException {
		this.conn = conn;
		getEmailAttachments = conn.prepareStatement(emailAttachmentsGet);
		getEmailAttachments.setInt(1, emailID);
   		ResultSet rs = getEmailAttachments.executeQuery();
   		List<AttachmentObj> attachmentList = new ArrayList<AttachmentObj>();
   		while(rs.next()) {
   			attachmentList.add(new AttachmentObj(rs.getString(2), rs.getString(3), rs.getBytes(4)));
   		}
   		emailInfo.setAttachment(attachmentList);
   		return emailInfo;
	}
	
	/**
	 * downloads all attachments based on a email id
	 * @param emailID
	 * @param conn
	 * @param dest
	 * @throws SQLException
	 */
	public void downloadAttachments(int emailID, Connection conn, File dest) throws SQLException {
		this.conn = conn;
	    FileOutputStream fos = null;
		getEmailAttachments = conn.prepareStatement(emailAttachmentsGet);
		getEmailAttachments.setInt(1, emailID);
   		ResultSet rs = getEmailAttachments.executeQuery();
   		List<AttachmentObj> attachmentList = new ArrayList<AttachmentObj>();
   		while(rs.next()) {
   			attachmentList.add(new AttachmentObj(rs.getString(2), rs.getString(3), rs.getBytes(4)));
   		}
   		for(AttachmentObj attachment : attachmentList) {
   			try (FileOutputStream fileOuputStream = new FileOutputStream(dest+"/"+attachment.getName()+"."+attachment.getType())) {
   	            fileOuputStream.write(attachment.getFile());
   	        } catch (IOException e) {
   	            e.printStackTrace();
   	        }
   		}
	}
}
