package persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import business.MailController;
import data.EmailBean;

public class EmailDBemailBean {
	
    private final static Logger LOG = LoggerFactory.getLogger(MailController.class);
	Connection conn;
	PreparedStatement updateEmailBean = null;
   	String emailBeanInsert = "Insert into emailBean"
   			+ "(emailId, emailSubject, textMessage, htmlMessage, sendDate, receiveDate, folder, priorty) values"
   			+ "(?,?,?,?,?,?,?,?)";
   	PreparedStatement getEmailBean = null;
   	String emailBeanGet = "Select * from emailBean where emailId = ?";
   	PreparedStatement changeFolder = null;
   	String changeFolerUpdate = "update emailbean set folder=? where emailId=?;";
   	PreparedStatement getIDs = null;
   	String getIDsSelect = "select * from emailBean;";
	PreparedStatement getIDsLink = null;
   	String getIDsSelectLink = "select * from emailBean where folder = ?;";
   	
   	
   	/**
   	 * gets all email ids
   	 * 
   	 * @param conn
   	 * @return list of valid ids
   	 * @throws SQLException
   	 */
   	public List<Integer> getEmailIDs(Connection conn) throws SQLException{
    	List<Integer> ids = new ArrayList<Integer>();
   		this.conn = conn;
   		getIDs = conn.prepareStatement(getIDsSelect);
   		ResultSet rs = getIDs.executeQuery();
   		while(rs.next()) {
   			ids.add(rs.getInt(1));
   		}
   		return ids;
   	}
   	
   	/**
   	 * Gets a list of all email ids
   	 * 
   	 * @param conn
   	 * @param link folder
   	 * @return list of all used ids
   	 * @throws SQLException
   	 */
   	public List<Integer> getEmailIDs(Connection conn, int link) throws SQLException {
   		List<Integer> ids = new ArrayList<Integer>();
   		this.conn = conn;
   		getIDsLink = conn.prepareStatement(getIDsSelectLink);
   		getIDsLink.setInt(1, link);
   		ResultSet rs = getIDsLink.executeQuery();
   		while(rs.next()) {
   			ids.add(rs.getInt(1));
   		}
   		return ids;
	}
   	
   	/**
   	 * places a bean into the emailBean table
   	 * 
   	 * @param emailInfo
   	 * @param emailID
   	 * @param conn
   	 * @throws SQLException
   	 */
   	public void insertIntoEmailBeanTable(EmailBean emailInfo, int emailID, int addressID, Connection conn) throws SQLException {
   		this.conn = conn;
   		updateEmailBean = conn.prepareStatement(emailBeanInsert);
       	updateEmailBean.setInt(1, emailID);
       	updateEmailBean.setString(2, emailInfo.getSubject().toString());
       	updateEmailBean.setString(3, emailInfo.getTextMessage().toString());
       	updateEmailBean.setString(4, emailInfo.getHtmlMessage().toString());
      	updateEmailBean.setString(5, emailInfo.getSendTime().toString());
       	updateEmailBean.setString(6, emailInfo.getReceiveTime().toString());
        updateEmailBean.setInt(7, emailInfo.getLink());
       	updateEmailBean.setInt(8, emailInfo.getPriorty());
       	updateEmailBean.executeUpdate();   		
   	}
   	
   	/**
   	 * returns a bean from the emailBean table
   	 * 
   	 * @param emailInfo
   	 * @param emailID
   	 * @param conn
   	 * @return a enailBean
   	 * @throws SQLException
   	 */
   	public EmailBean getFromEmailBeanTable(EmailBean emailInfo, int emailID, Connection conn) throws SQLException {
   		this.conn = conn;
   		getEmailBean = conn.prepareStatement(emailBeanGet);
   		getEmailBean.setInt(1, emailID);
   		ResultSet rs = getEmailBean.executeQuery();
   		while(rs.next()) {
   			emailInfo.setEmailID(rs.getInt(1));
   			emailInfo.setSubject(rs.getString(2));
   			emailInfo.setTextMessage(new ArrayList<String>(Arrays.asList(rs.getString(3).split(",")[0].substring(1),rs.getString(3).split(",")[1].substring(1),rs.getString(3).split(",")[2].split("]")[0].substring(1))));
   			emailInfo.setHtmlMessage(new ArrayList<String>(Arrays.asList(rs.getString(4).split(",")[0].substring(1),rs.getString(4).split(",")[1].substring(1),rs.getString(4).split(",")[2].split("]")[0].substring(1))));
   			emailInfo.setTypeOfMessage("new");
   			emailInfo.setLink(rs.getInt(7));
   			emailInfo.setSendTime(rs.getString(5));
   			emailInfo.setReceiveTime(rs.getString(6));
   			emailInfo.setPriorty(rs.getInt(8));
   		}
   		return emailInfo;
   	}
   	
   	/**
   	 * Changes the folder of the bean in the bean table
   	 * 
   	 * @param emailID
   	 * @param folderID
   	 * @param conn
   	 * @throws SQLException
   	 */
   	public void changeFolder(int emailID, int folderID, Connection conn) throws SQLException {
   		this.conn = conn;
   		changeFolder = conn.prepareStatement(changeFolerUpdate);
   		changeFolder.setInt(1, folderID);
   		changeFolder.setInt(2, emailID);
   		changeFolder.executeUpdate();
   	}

}
