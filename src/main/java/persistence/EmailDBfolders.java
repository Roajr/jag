package persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import business.MailController;

public class EmailDBfolders {
	
    private final static Logger LOG = LoggerFactory.getLogger(MailController.class);
	
	Connection conn;
	PreparedStatement findFolder = null;
	String findFolderStatement = "Select * from folderstable where foldername = ?";
	PreparedStatement makeFolder = null;
	String makeNewFolder = "insert into foldersTable(folderID, folderName) values"
    		+"(?,?);";
	PreparedStatement getFolderName = null;
	String getFolderNameString = "Select * from foldersTable where folderID = ?";
	PreparedStatement removeFolder = null;
	String removeFolderString = "Delete from foldersTable where folderID = ?";
	
	/**
	 * makes the inbox and send folders in the database
	 * 
	 * @param conn
	 * @throws SQLException
	 */
	public void makeDefaultFolders(Connection conn) throws SQLException {
		this.conn = conn;
		Statement stat = conn.createStatement();
		String Query = "insert into foldersTable(folderID, folderName) values"
        		+"(1,'sent');";
        stat.executeUpdate(Query);
        Query = "insert into foldersTable(folderID, folderName) values"
            		+"(2,'inbox');";
        stat.executeUpdate(Query);
	}
	
	/**
	 * finds a folder by name in the folders table
	 * 
	 * @param folderName
	 * @param conn
	 * @return folder id if present
	 * @throws SQLException
	 */
	public int isFolderPresent (String folderName, Connection conn) throws SQLException {
		this.conn = conn;
		findFolder = conn.prepareStatement(findFolderStatement);
		findFolder.setString(1, folderName);
		ResultSet rs = findFolder.executeQuery();
		while(rs.next()) {
			if(rs.getString(2) == folderName);
			return rs.getInt(1);
		}
		return 0;
	}
	
	/**
	 * makes a new folder in the folder table and returns the id
	 * 
	 * @param folderID
	 * @param folderName
	 * @param conn
	 * @return id of the new folder
	 * @throws SQLException
	 */
	public int makeNewFolder(int folderID, String folderName, Connection conn) throws SQLException {
		int folderPresent = isFolderPresent(folderName, conn);
		if(folderPresent == 0) {
			this.conn = conn;
			makeFolder = conn.prepareStatement(makeNewFolder);
			makeFolder.setInt(1, folderID);
			makeFolder.setString(2, folderName);
			makeFolder.executeUpdate();	
			return folderID;
		}
		else {
			folderPresent = -1;
			return folderPresent;
		}
	}
	
	/**
	 * Gets the folder name bassed on its id
	 * 
	 * @param folderID
	 * @param conn
	 * @return folder name
	 * @throws SQLException
	 */
	public String getFolderName(int folderID, Connection conn) throws SQLException {
		String name = "";
		this.conn = conn;
		getFolderName = conn.prepareStatement(getFolderNameString);
		getFolderName.setInt(1, folderID);
		ResultSet rs = getFolderName.executeQuery();
		while(rs.next()) {
			name = rs.getString(2);
		}
		return name;
	}
	
	/**
	 * removes the folder from the folder table
	 * @param folderID
	 * @param conn
	 * @throws SQLException
	 */
	public void removeFolder(int folderID, Connection conn) throws SQLException {
		this.conn = conn;
		removeFolder = conn.prepareStatement(removeFolderString);
		removeFolder.setInt(1, folderID);
		removeFolder.executeUpdate();
	}
}
