package app;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Properties;
import java.util.ResourceBundle;

import business.DatabaseController;
import business.MailController;
import data.AttachmentObj;
import data.EmailBean;
import data.LoginBeanFx;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.Window;
import presentation.LoginController;
import presentation.RootController;
import presentation.TreeController;

/**
 * Main class for jag app
 * 
 * @author Jamroa
 *
 */
public class Jag extends Application{
    private final static Logger LOG = LoggerFactory.getLogger(MailController.class);
    private DatabaseController dbManager;
    private Properties prop;
    private Locale currentLocale;
    private Stage window;
    
	/**
	 * Main method to run program and calls the UI controller
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
    	launch(args);
	}

	/**
	 * loads the login scene on the window
	 */
	private void loadLogin() {
		try {
	           FXMLLoader loader = new FXMLLoader();
	           loader.setResources(ResourceBundle.getBundle("MessagesBundle", currentLocale));
	           loader.setLocation(getClass().getResource("/fxml/loginLayout.fxml"));
	           Parent root = loader.load();
	           LoginController loginControl = loader.getController();
	           Scene scene = new Scene(root);
	           window.setTitle("Login JAG");
	           window.setScene(scene);
	           window.show();
	           window.setTitle("Login JAG");
	           window.setScene(scene);
	           window.showAndWait();
	       } catch (IOException | IllegalStateException ex) {
	           LOG.error("Unable to load fxml", ex);
	           LOG.error(ex.getMessage());
	    }
	}
	
	/**
	 * Loads the mailApp scene on the window
	 * 
	 * @throws SQLException
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void loadMainApp() throws SQLException, IOException, InterruptedException {
    	try {
    		Stage window = new Stage(); 
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(ResourceBundle.getBundle("MessagesBundle", currentLocale));
            loader.setLocation(getClass().getResource("/fxml/rootLayout.fxml"));
            Parent root = loader.load();
            RootController controller = loader.getController();
            controller.setRoot(this);
            Scene scene = new Scene(root);
            window.setTitle("JAG");
            window.setScene(scene);
            window.show();
        } catch (IOException | IllegalStateException ex) {
            LOG.error("Unable to load fxml", ex);
            LOG.error(ex.getMessage());
        }		
	}
	
	/**
	 * Sets up the window
	 */
	public void start(Stage primaryStage) throws IOException, SQLException, InterruptedException {
		window = primaryStage;
        currentLocale = new Locale("en","CA");
		        
    	Platform.setImplicitExit(false);
		Path propertiespath = Paths.get("jag.properties");
    	if (Files.exists(propertiespath)) {
    		try {
    			prop = new Properties();
				InputStream propInput = new FileInputStream("jag.properties");
				prop.load(propInput);
				dbManager = new DatabaseController(prop);
				loadMainApp();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				LOG.error("Cannot read properties file");
				e.printStackTrace();
			}
    	}else {
    		LOG.debug("Cannot find properties file");
    		//make new props file
    		loadLogin();
       	}
	}

	/**
	 * Changes the locat of the app from the root controller
	 * 
	 * @param local
	 * @throws IOException
	 * @throws SQLException
	 * @throws InterruptedException
	 */
	public void setLocal(Locale local) throws IOException, SQLException, InterruptedException {
		window.close();
		this.currentLocale = local;
		loadMainApp();
	}
}
