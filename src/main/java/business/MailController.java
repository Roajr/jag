package business;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import data.AttachmentObj;
import data.EmailBean;
import jodd.mail.Email;
import jodd.mail.EmailAddress;
import jodd.mail.EmailAttachment;
import jodd.mail.EmailMessage;
import jodd.mail.ImapServer;
import jodd.mail.MailServer;
import jodd.mail.RFC2822AddressParser;
import jodd.mail.ReceiveMailSession;
import jodd.mail.ReceivedEmail;
import jodd.mail.SendMailSession;
import jodd.mail.SmtpServer;
import logic.EmailCreate;

/**
 * Class to handle general email business logic
 * 
 * @author Jamroa
 */
public class MailController {
	
    private final static Logger LOG = LoggerFactory.getLogger(MailController.class);

	private EmailBean emailInfo;
	//Email Logic class creators
	private EmailCreate emailManager = new EmailCreate();
	private DatabaseController dbManager;
	private final String smtpServerName = "smtp.gmail.com";
	private final String imapServerName = "imap.gmail.com";
	
	/**
	 * No parameter constructor for the mail Controller
	 */
	public MailController() {
		setDbManager();
		this.emailInfo = new EmailBean();
	}
	
	/**
	 * Basic constructor that sets the emailBean
	 * 
	 * @param email1
	 */
	public MailController(EmailBean email) {
		setDbManager();
		this.emailInfo = email;
	}
	
	/**
	 * Given the email and password will send a email based on the emailBean
	 * 
	 * @param fromEmail
	 * @param fromPwd
	 * @return
	 * @throws SQLException 
	 */
	public boolean sendEmail(String fromEmail ,String fromPwd, EmailBean emailData) throws SQLException {
		this.emailInfo = emailData;
	//Makes sure that there is a from email address, a from email pwd and at least one to address to send the most basic default email
		
		LOG.debug(this.emailInfo.getFromEmail());
		LOG.debug(fromEmail);

		if(this.emailInfo.getFromEmail() != null && fromPwd != null && this.emailInfo.getToEmail().size() > 0 /*&& this.emailInfo.getFromEmail() == fromEmail*/)
		{			
			if(checkEmail(this.emailInfo.getFromEmail())) {
				for(String toEmails : this.emailInfo.getToEmail()) {
					if(!checkEmail(toEmails)) {
						break;
					}
				}
				SmtpServer smtpServer = getSmtpServer(emailInfo.getFromEmail(),fromPwd);
				Email email = emailManager.makeEmail(emailInfo);
 				try(SendMailSession session = smtpServer.createSession()){
					session.open();
					session.sendMail(email);
					return true; 
				}
	        }else {
	        	LOG.debug("Unable to send email because either send or recieve addresses are invalid");
				return false;
	        }
				
		}
		else {
			LOG.debug("Email does not have a needed to and get email address");
			return false;
		}
	}
	
	/**
	 * Sets up the dbManager off the properties file
	 */
	public void setDbManager() {
    	Path propertiespath = Paths.get("jag.properties");
    	if (Files.exists(propertiespath)) {
    		try {
    			Properties prop = new Properties();
				InputStream propInput = new FileInputStream("jag.properties");
				prop.load(propInput);
				dbManager = new DatabaseController(prop);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				LOG.debug("Cannot read properties file");
				e.printStackTrace();
			}
    	}else {
    		LOG.debug("Cannot find properties file");
    		//make new props file
       	}
    }
	
	/**
	 * Given the email and password will receive all email's mark then as delete and return the list of email beans
	 * 
	 * @param toEmail
	 * @param toPwd
	 * @return
	 * @throws IOException 
	 */
	public List<EmailBean> receiveMail(String toEmail,String toPwd) throws IOException {
		List<EmailBean> receivedEmails = new ArrayList<EmailBean>();
		if(checkEmail(toEmail)) {
			ImapServer imapServer = getImapServer(toEmail, toPwd);
			try(ReceiveMailSession session = imapServer.createSession()){
				session.open();
				LOG.debug(String.valueOf(session.getMessageCount()));
				ReceivedEmail[] emails = session.receiveEmailAndDelete();
				if (emails != null) {
                    for (ReceivedEmail email : emails) {
                    	receivedEmails.add(processReceivedEmail(email));
                    }
                    return receivedEmails;
                }
			}
		}else {
            LOG.debug("Unable to send email because either send or recieve addresses are invalid");
        }
		return null;
	}

	
	/**
	 * Process the received email information of a received email
	 * 
	 * @param email
	 * @return
	 * @throws IOException 
	 */
	private EmailBean processReceivedEmail(ReceivedEmail email) throws IOException {
		EmailBean receiveEmail = new EmailBean();
        receiveEmail.setFromEmail(email.from().toString());
        List<String> toList = new ArrayList<>();
        for(EmailAddress toEmails : email.to()) {
        	toList.add(toEmails.toString());
        }
        receiveEmail.setToEmail(toList);
        List<String> ccList = new ArrayList<>();
        for(EmailAddress ccEmails : email.cc()) {
        	ccList.add(ccEmails.toString());
        }
        receiveEmail.setCcEmail(ccList);
        List<String> bccList = new ArrayList<>();
        receiveEmail.setBccEmail(bccList);
        receiveEmail.setSubject(email.subject());
        receiveEmail.setPriorty(email.priority());
        receiveEmail.setSendTime(email.sentDate().toString());
        receiveEmail.setReceiveTime(email.receivedDate().toString());

        List<EmailMessage> messages = email.messages();
        receiveEmail = processMessages(receiveEmail, messages);
     
        List<EmailAttachment<? extends javax.activation.DataSource>> attachments = email.attachments();
        receiveEmail = processAttachments(receiveEmail, attachments);
        	
        receiveEmail.setTypeOfMessage("Received");
        receiveEmail.setLink(2);
        return(receiveEmail);
	}
	
	/**
	 * Process the messages of the received email
	 * 
	 * @param receiveEmail
	 * @param messages
	 * @return
	 */
	private EmailBean processMessages(EmailBean receiveEmail, List<EmailMessage> messages) {
		for(EmailMessage messageFill : messages) {
        	List<String> tempMessage = new ArrayList<String>();
        		tempMessage.add(messageFill.getEncoding());
        		tempMessage.add(messageFill.getMimeType());
        		tempMessage.add(messageFill.getContent());
        		switch (messageFill.getMimeType()) {
        			case "TEXT/PLAIN":
        				receiveEmail.setTextMessage(tempMessage);
        				break;
        			case "TEXT/HTML":
        				String tempString = tempMessage.get(2);
        				tempString = tempString.substring(0, tempString.length()-14);
        				tempString = tempMessage.get(2);
        				boolean src = true;
        				while (src) {
        					if(tempString.contains("src")) {
        						int start = tempString.indexOf("src");
        						int end = tempString.indexOf("\"", start+6);
        						tempString = tempString.substring(0, start) + tempString.substring(end+2, tempString.length());
        					}else {
        						src = false;
        					}
        				}       				
        				String finalString = tempString.replaceAll("alt", "src");
        				tempMessage.remove(2);
        				tempMessage.add(finalString);  
        				receiveEmail.setHtmlMessage(tempMessage);
        				break;
        			default:
        				LOG.debug("Message has off message type");
        		}
        }	
		return receiveEmail;
	}
	
	/**
	 * Process the attachments of the received email
	 * 
	 * @param receiveEmail
	 * @param attachments
	 * @return
	 * @throws IOException 
	 */
	private EmailBean processAttachments(EmailBean receiveEmail, List<EmailAttachment<? extends javax.activation.DataSource>> attachments) throws IOException {
		List<AttachmentObj> receivedAttachments = new ArrayList<AttachmentObj>();
	    if (attachments != null) {
	      	for(EmailAttachment attachmentFill : attachments) {
	      		FileOutputStream stream = new FileOutputStream(attachmentFill.getEncodedName());
	      		stream.write(attachmentFill.toByteArray());
	       		receivedAttachments.add(new AttachmentObj(attachmentFill.getName().split("\\.")[0] ,attachmentFill.getName().split("\\.")[1] ,attachmentFill.toByteArray()));
	       	}
	       	receiveEmail.setAttachment(receivedAttachments);
	    }
		return receiveEmail;
	}
	
	/**
	 * will check a email Address and make sure it is valid
	 * 
	 * @param address
	 * @return
	 */
	private boolean checkEmail(String address) {
        return RFC2822AddressParser.STRICT.parseToEmailAddress(address) != null;    
	}
	
	/**
	 * Creates a smtp server with the supplied email and password
	 * 
	 * @param sendingEmail smtp email
	 * @param sendingEmailPwd emails password
	 * @return smtp server
	 */
	public SmtpServer getSmtpServer(String sendingEmail, String sendingEmailPwd) {
		SmtpServer smtpServer = MailServer.create()
                .ssl(true)
                .host(smtpServerName)
                .auth(sendingEmail, sendingEmailPwd)
                .debugMode(true)
                .buildSmtpMailServer();
		return smtpServer;
	}
	
	/**
	 * Creates a imap server with the supplied email and password
	 * 
	 * @param receiveingEmail imap email
	 * @param receiveingEmailPwd emails password
	 * @return imap server
	 */
	private ImapServer getImapServer(String receiveingEmail, String receiveingEmailPwd) {
        ImapServer imapServer = MailServer.create()
				.host(imapServerName)
                .ssl(true)
                .auth(receiveingEmail, receiveingEmailPwd)
                .buildImapMailServer();
        return imapServer;
	}
	
}
