package business;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import data.AttachmentObj;
import data.EmailBean;
import data.EmailBeanFx;
import data.FolderBeanFx;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import persistence.EmailDBattachments;
import persistence.EmailDBemailAddress;
import persistence.EmailDBemailBean;
import persistence.EmailDBemailBridge;
import persistence.EmailDBfolders;

/**
 * Class to handle all database logic
 * 
 * @author Jamroa
 */
public class DatabaseController {
    private final static Logger LOG = LoggerFactory.getLogger(DatabaseController.class);    
    private EmailDBemailBean dbBean;
    private EmailDBattachments dbAttachment;
    private EmailDBfolders dbFolder;
    private EmailDBemailAddress dbaddress;
    private EmailDBemailBridge dbaddressbridge;
    private Properties prop;
    private InputStream propInput;
    
    /**
     * Default constructor
     * @param prop
     * @throws IOException
     */
	public DatabaseController(Properties prop) throws IOException {
		this.prop = prop;
		dbBean = new EmailDBemailBean();
    	dbAttachment = new EmailDBattachments();
    	dbFolder = new EmailDBfolders();
    	dbaddress = new EmailDBemailAddress();
    	dbaddressbridge = new EmailDBemailBridge();	
   	}
	
	/**
	 * Will update the database with all the information of a emailBean
	 * 
	 * @param emailInfo a emailBean
	 * @throws SQLException
	 */
    public void create(EmailBean emailInfo) throws SQLException{
    	int emailID;   
    	int addressID;
    	try(Connection conn = DriverManager.getConnection(prop.getProperty("URL"),prop.getProperty("USER"), prop.getProperty("PASSWORD"))){     	
    	   	emailID = getLastEmailBeanID();
   			addressID = getLastAddressID(); 
    	   	conn.setAutoCommit(false);
    	   	if(dbaddress.insertIntoEmailAddressTable(dbaddressbridge, emailInfo.getFromEmail(), addressID, emailID, "FR", conn)) {
       			addressID++;
       		}
        	dbBean.insertIntoEmailBeanTable(emailInfo, emailID, addressID, conn);            	
           	for(AttachmentObj attachment : emailInfo.getAttachment()) {
           		dbAttachment.insertIntoAttachmentsTable(attachment, emailID, conn);
           	}
           	//Start of the address tables
   			for(String toEmail : emailInfo.getToEmail()) {          	
           		if(dbaddress.insertIntoEmailAddressTable(dbaddressbridge, toEmail, addressID, emailID, "TO", conn)) {
           			addressID++;
           		}
           	}   
   			for(String CCEmail : emailInfo.getCcEmail()) {          	
   				if(dbaddress.insertIntoEmailAddressTable(dbaddressbridge, CCEmail, addressID, emailID, "CC", conn)) {
   	       			addressID++;
   				}
           	} 
   			for(String BCCEmail : emailInfo.getBccEmail()) {          	
   				if(dbaddress.insertIntoEmailAddressTable(dbaddressbridge, BCCEmail, addressID, emailID, "BC",conn)) {
   	       			addressID++;
   				}
           	} 
           	conn.commit();
        }
    }
    
    /**
     * Will remove a email from the database and all on all tables other then
     * the email address table because the address may be used again
     * 
     * @param emailID of a email
     * @throws SQLException
     */
    public void delete(int emailID) throws SQLException {
    	try(Connection conn = DriverManager.getConnection(prop.getProperty("URL"),prop.getProperty("USER"), prop.getProperty("PASSWORD"))){     	
    		Statement stat = conn.createStatement();
    		String Query;
    		Query = "delete from emailaddressbridge where emailaddressbridge.emailKey = " + emailID; 
    		stat.executeUpdate(Query);
    		Query = "delete from emailbean where emailbean.emailId = " + emailID;
    		stat.executeUpdate(Query);
    		Query = "delete from attachmentstable where attachmentstable.attachmentEmail = " + emailID;
    		stat.executeUpdate(Query);
    	}
    }
    
    /**
     * will change the email to a different folder in the database
     * 
     * @param emailID of a email
     * @param folderName of the folder you want to change it to
     * @throws SQLException
     */
    public void changeFolder(int emailID, String folderName) throws SQLException {
    	int folderID;
    	try(Connection conn = DriverManager.getConnection(prop.getProperty("URL"),prop.getProperty("USER"), prop.getProperty("PASSWORD"))){     	
    		folderID = dbFolder.isFolderPresent(folderName, conn);
        	if(folderID != 0) {
        		dbBean.changeFolder(emailID, folderID, conn);
        	}else {
        		LOG.debug("That folder does not existe but we just made it");
        		folderID = dbFolder.makeNewFolder(getLastFolderID(), folderName, conn);
        		dbBean.changeFolder(emailID, folderID, conn);
        	}
    	}
    }
    
    /**
     * will make a new folder and change the id to it
     * 
     * @param emailID of a email
     * @param folderNameof the folder you want to make
     * @throws SQLException
     */
    public void changeNewFolder(int emailID, String folderName) throws SQLException {
    	int folderID;
    	try(Connection conn = DriverManager.getConnection(prop.getProperty("URL"),prop.getProperty("USER"), prop.getProperty("PASSWORD"))){     	
    		folderID = dbFolder.isFolderPresent(folderName, conn);
        	if(folderID != 0) {
        		LOG.debug("folder is present, email changed to existing folder");
        		dbBean.changeFolder(emailID, folderID, conn);
        	}else {
        		folderID = dbFolder.makeNewFolder(getLastFolderID(), folderName, conn);
        		dbBean.changeFolder(emailID, folderID, conn);
        	}
    	}
    }
    
    /**
     * Crates a new folder in the folder table based on folder name
     * @param folderName
     * @throws SQLException
     */
    public void makeNewFolder(String folderName) throws SQLException {
    	try(Connection conn = DriverManager.getConnection(prop.getProperty("URL"),prop.getProperty("USER"), prop.getProperty("PASSWORD"))){     	
    		int folderID = dbFolder.makeNewFolder(getLastFolderID(), folderName, conn);
    	}
    }
    
    /**
     * downloads all attachments for a given ID
     * @param emailID
     * @param dest
     * @throws SQLException
     */
    public void downloadAttachments(int emailID, File dest) throws SQLException {
    	try(Connection conn = DriverManager.getConnection(prop.getProperty("URL"),prop.getProperty("USER"), prop.getProperty("PASSWORD"))){     	
    		dbAttachment.downloadAttachments(emailID, conn, dest);
    	}
    }
    
    /**
     * Gets the emailBean that matches the emailID 
     * 
     * @param emailID
     * @return the emailBean with that id
     * @throws SQLException
     */
    public EmailBean findID(int emailID) throws SQLException {
    	try(Connection conn = DriverManager.getConnection(prop.getProperty("URL"),prop.getProperty("USER"), prop.getProperty("PASSWORD"))){     	
    		EmailBean foundEmail = new EmailBean();
    		foundEmail = dbBean.getFromEmailBeanTable(foundEmail, emailID, conn);
    		foundEmail = dbAttachment.getFromAttachmentsTable(foundEmail, emailID, conn);
    		foundEmail = dbaddress.getFromEmail(foundEmail, emailID, conn);
    		foundEmail = dbaddress.getToEmail(foundEmail, emailID, conn);
    		foundEmail = dbaddress.getCcEmail(foundEmail, emailID, conn);
    		foundEmail = dbaddress.getBcEmail(foundEmail, emailID, conn); 
        	return foundEmail;
    	}

    }
    
    /**
     * get a list of all emailBeans that are in the database
     * 
     * @return a list of emailbeans
     * @throws SQLException
     */
    public List<EmailBean> findAll() throws SQLException{
    	List<Integer> ids = new ArrayList<Integer>();
    	List<EmailBean> emails = new ArrayList<EmailBean>();
    	try(Connection conn = DriverManager.getConnection(prop.getProperty("URL"),prop.getProperty("USER"), prop.getProperty("PASSWORD"))){     	
    		ids = dbBean.getEmailIDs(conn);
    		for(int emailID : ids) {
    			emails.add(findID(emailID));
    		}
    		return emails;
    	}
    }
    
    /**
     * override for the FindAll method that only looks for values in a folder
     * 
     * @param link folder id
     * @return a list of emailbeans
     * @throws SQLException
     */
    public List<EmailBean> findAll(int link) throws SQLException{
    	List<Integer> ids = new ArrayList<Integer>();
    	List<EmailBean> emails = new ArrayList<EmailBean>();
    	try(Connection conn = DriverManager.getConnection(prop.getProperty("URL"),prop.getProperty("USER"), prop.getProperty("PASSWORD"))){     	
    		ids = dbBean.getEmailIDs(conn, link);
    		for(int emailID : ids) {
    			emails.add(findID(emailID));
    		}
    		return emails;
    	}
    }
    
    /**
     * Gets the next available emailAddress id for the emailAddress table 
     * 
     * @return int id for address
     * @throws SQLException
     */
    public int getLastAddressID() throws SQLException {
    	int id = 0;
    	try(Connection conn = DriverManager.getConnection(prop.getProperty("URL"),prop.getProperty("USER"), prop.getProperty("PASSWORD"))){     	
    		Statement stat = conn.createStatement();
    		String Query = "select MAX(emailAddressID) from emailaddresstable;";
    		ResultSet rs = stat.executeQuery(Query);
    		while(rs.next()) {
    			id = rs.getInt(1);
    			id++;
    		}
    	}
    	return id;
    }
    
    /**
     * Gets the next available emailBean id for the emailbean table 
     * 
     * @return int id for emailBean
     * @throws SQLException
     */
    public int getLastEmailBeanID() throws SQLException {
    	int id = 0;
    	try(Connection conn = DriverManager.getConnection(prop.getProperty("URL"),prop.getProperty("USER"), prop.getProperty("PASSWORD"))){     	
    		Statement stat = conn.createStatement();
    		String Query = "select MAX(emailId) from emailbean;";
    		ResultSet rs = stat.executeQuery(Query);
    		while(rs.next()) {
    			id = rs.getInt(1);
    			id++;
    		}
    	}
    	return id;
    }
    
    /**
     * Gets the next available folder id for the emailbean table 
     * 
     * @return int id for emailBean
     * @throws SQLException
     */
    public int getLastFolderID() throws SQLException {
    	int id = 0;
    	try(Connection conn = DriverManager.getConnection(prop.getProperty("URL"),prop.getProperty("USER"), prop.getProperty("PASSWORD"))){     	
    		Statement stat = conn.createStatement();
    		String Query = "select MAX(folderID) from folderstable;";
    		ResultSet rs = stat.executeQuery(Query);
    		while(rs.next()) {
    			id = rs.getInt(1);
    			id++;
    		}
    	}
    	return id;
    }
    
    /**
     * Get a list of all the folders for the javafx folder tree
     * 
     * @return list of strings that are folders in the db
     * @throws SQLException
     */
    public ObservableList<FolderBeanFx> getFolders() throws SQLException {
    	ObservableList<FolderBeanFx> folder = FXCollections.observableArrayList();
		String Query = "select * from folderstable;";

    	try(Connection conn = DriverManager.getConnection(prop.getProperty("URL"),prop.getProperty("USER"), prop.getProperty("PASSWORD"))){     
    		
    		Statement stat = conn.createStatement();
    		ResultSet rs = stat.executeQuery(Query);
    		while(rs.next()) {
    			//folder.add(rs.getString(1));
    			folder.add(createFolderBeanFx(rs));
    		}
    	}
    	return folder;
    }
    
    /**
     * Get a list of all the emails for the javafx table
     * 
     * @return list of strings that are folders in the db
     * @throws SQLException
     */
    public ObservableList<EmailBeanFx> FindTableAll() throws SQLException{
    	List<EmailBean> emailBeans = findAll();
    	ObservableList<EmailBeanFx> emails = FXCollections.observableArrayList();
    	for(EmailBean email : emailBeans) {
			EmailBeanFx fxbean = new EmailBeanFx(email.getEmailID(), email.getFromEmail(), email.getToEmail(), email.getCcEmail(), email.getBccEmail(), email.getSubject(), email.getTextMessage(), email.getHtmlMessage(), email.getAttachment(), email.getTypeOfMessage(), email.getLink(), email.getSendTime(), email.getReceiveTime(), email.getPriorty()); 
    		if(fxbean.getReceiveTimeFx() == null) {
    			fxbean.setReceiveTimeFx("");
    		}
    		emails.add(fxbean);
    	}
    	return emails;
    }
    
    /**
     * Override for findTableAll 
     * Get a list of all the emails for the javafx table that are in a folder
     * 
     * @return list of strings that are folders in the db
     * @throws SQLException
     */
    public ObservableList<EmailBeanFx> FindTableAll(int link) throws SQLException{
    	List<EmailBean> emailBeans = findAll(link);
    	ObservableList<EmailBeanFx> emails = FXCollections.observableArrayList();
    	for(EmailBean email : emailBeans) {
			EmailBeanFx fxbean = new EmailBeanFx(email.getEmailID(), email.getFromEmail(), email.getToEmail(), email.getCcEmail(), email.getBccEmail(), email.getSubject(), email.getTextMessage(), email.getHtmlMessage(), email.getAttachment(), email.getTypeOfMessage(), email.getLink(), email.getSendTime(), email.getReceiveTime(), email.getPriorty()); 
    		if(fxbean.getReceiveTimeFx() == null) {
    			fxbean.setReceiveTimeFx("");
    		}
    		emails.add(fxbean);
    	}
    	return emails;
    }
    
    /**
     * creates a folderBeanFx based on a resultset value
     * 
     * @param rs
     * @return
     * @throws SQLException
     */
    public FolderBeanFx createFolderBeanFx(ResultSet rs) throws SQLException {
    	FolderBeanFx folderBeanFx = new FolderBeanFx();
    	folderBeanFx.setId(rs.getInt(1));
    	folderBeanFx.setFolderName(rs.getString(2));
    	return folderBeanFx;
    }
    
    /**
     * Gets the folder name from its id off the database
     * 
     * @param folderId
     * @return folder name
     * @throws SQLException
     */
    public String getFolderName(int folderId) throws SQLException {
    	try(Connection conn = DriverManager.getConnection(prop.getProperty("URL"),prop.getProperty("USER"), prop.getProperty("PASSWORD"))){     	
    		return dbFolder.getFolderName(folderId, conn);
    	}
    }
    
    /**
     * Gets folder id from the folder name
     * @param folderName
     * @return
     * @throws SQLException
     */
    public int getFolderId(String folderName) throws SQLException {
    	try(Connection conn = DriverManager.getConnection(prop.getProperty("URL"),prop.getProperty("USER"), prop.getProperty("PASSWORD"))){     	
    		return dbFolder.isFolderPresent(folderName, conn);
    	}
    }
    
    /**
     * removes folder that corresponds to the folder id
     * @param folderID
     * @throws SQLException
     */
	public void removeFolder(int folderID) throws SQLException {
    	try(Connection conn = DriverManager.getConnection(prop.getProperty("URL"),prop.getProperty("USER"), prop.getProperty("PASSWORD"))){     	
    		dbFolder.removeFolder(folderID, conn);
    	}		
	} 
    
    /**
     * Will drop all tables then recreate new tables to replace them
     * Will also make two folders inbox and send that are the default tables
     * 
     * @throws SQLException
     */
    public void createDatabaseTables() throws SQLException{
    	try(Connection conn = DriverManager.getConnection(prop.getProperty("URL"),prop.getProperty("USER"), prop.getProperty("PASSWORD"))){     	
        	Statement stat = conn.createStatement();
        	String Query;
        	Query="drop table if exists emailAddressBridge";
        	stat.executeUpdate(Query);
        	Query="drop table if exists attachmentsTable";
        	stat.executeUpdate(Query);
        	Query="drop table if exists emailAddressTable";
        	stat.executeUpdate(Query);
        	Query="drop table if exists emailBean";
        	stat.executeUpdate(Query);
        	Query="drop table if exists foldersTable";
        	stat.executeUpdate(Query);
       	}
    	try(Connection conn = DriverManager.getConnection(prop.getProperty("URL"),prop.getProperty("USER"), prop.getProperty("PASSWORD"))){     	
        	Statement stat = conn.createStatement();
        	String Query ="create table foldersTable(" +
        			"folderID INT," +
        			"folderName varchar(100)," +
        			"PRIMARY KEY (folderID));";
        	stat.executeUpdate(Query);
        	dbFolder.makeDefaultFolders(conn);
    	}
    	try(Connection conn = DriverManager.getConnection(prop.getProperty("URL"),prop.getProperty("USER"), prop.getProperty("PASSWORD"))){     	
        	Statement stat = conn.createStatement();
        	String Query="create table emailBean (" + 
        			"emailId INT NOT NULL," + 
        			"fromEmail varchar(254)," + 
        			"emailSubject text," + 
        			"textMessage text," + 
        			"htmlMessage text," + 
        			"sendDate varchar(32)," + 
        			"receiveDate varchar(32)," + 
        			"folder int references folderstable(folderID)," + 
        			"priorty int," + 
        			"PRIMARY KEY (emailId));";
        	stat.executeUpdate(Query);
       	}
    	try(Connection conn = DriverManager.getConnection(prop.getProperty("URL"),prop.getProperty("USER"), prop.getProperty("PASSWORD"))){     	
        	Statement stat = conn.createStatement();
        	String Query ="create table attachmentsTable(" +
        			"attachmentEmail INT," +
        			"attachmentName varchar(254)," +
        			"attachmentType varchar(4)," +
        			"attachmentData MEDIUMBLOB);";
        	stat.executeUpdate(Query);
    	}
    	try(Connection conn = DriverManager.getConnection(prop.getProperty("URL"),prop.getProperty("USER"), prop.getProperty("PASSWORD"))){     	
        	Statement stat = conn.createStatement();
        	String Query ="create table emailAddressTable(" +
        			"emailAddressID INT references emailBean(emailId)," +
        			"emailAddress varchar(254)," +
        			"PRIMARY KEY (emailAddressID));";
        	stat.executeUpdate(Query);
    	}
    	try(Connection conn = DriverManager.getConnection(prop.getProperty("URL"),prop.getProperty("USER"), prop.getProperty("PASSWORD"))){     	
        	Statement stat = conn.createStatement();
        	String Query ="create table emailAddressBridge(" +
        			"emailKey INT references emailbean(emailID)," +
        			"addressKey int references emailAddressTable(emailAddressID)," +
        			"type varchar(2));";
        	stat.executeUpdate(Query);
    	}
    }  
}
