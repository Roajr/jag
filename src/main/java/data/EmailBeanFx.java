package data;

import javafx.beans.property.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import business.DatabaseController;
import business.MailController;


public class EmailBeanFx {
	
    private final static Logger LOG = LoggerFactory.getLogger(MailController.class);

	//each has a getter a setter and a property 
	private IntegerProperty emailIDFx;
	private StringProperty fromEmailFx;
	private StringProperty toEmailFx;
	private StringProperty ccEmailFx;
	private StringProperty bccEmailFx;
	private StringProperty subjectFx;
	private StringProperty textMessageFx;
	private StringProperty htmlMessageFx;
	private StringProperty attachmentFx;
	private StringProperty typeOfMessageFx;
	private StringProperty linkFx;
	private StringProperty sendTimeFx;
	private StringProperty receiveTimeFx;
	private IntegerProperty priorty;
	
    private DatabaseController dbManager;
    
	public EmailBeanFx() throws SQLException {
		this(-1, "", new ArrayList<String>(), new ArrayList<String>(), new ArrayList<String>(), "", new ArrayList<String>(), new ArrayList<String>(), new ArrayList<AttachmentObj>(), "", -1, "", "", -1);
	}
	
	public EmailBeanFx(int emailID, 
			String fromEmail, 
			List<String> toEmail,
			List<String> ccEmail, 
			List<String> bccEmail,
			String subject,
			List<String> textMessage,
			List<String> htmlMessage, 
			List<AttachmentObj> attachment,
			String typeOfMessage,
			int link,
			String sendTime,
			String receiveTime, 
			int priorty) throws SQLException {
			if (receiveTime == null) {
				receiveTime = "";
				}
			
		setDbManager();
		this.emailIDFx = new SimpleIntegerProperty(emailID);
		this.fromEmailFx = new SimpleStringProperty(fromEmail);
		this.toEmailFx = new SimpleStringProperty(toEmail.toString());
		this.ccEmailFx = new SimpleStringProperty(ccEmail.toString());
		this.bccEmailFx = new SimpleStringProperty(bccEmail.toString());
		this.subjectFx = new SimpleStringProperty(subject);
		this.textMessageFx = new SimpleStringProperty(textMessage.toString());
		this.htmlMessageFx = new SimpleStringProperty(htmlMessage.toString());
		this.attachmentFx = new SimpleStringProperty(attachment.toString());
		this.typeOfMessageFx = new SimpleStringProperty(typeOfMessage);
		if(link == -1) {
			this.linkFx = new SimpleStringProperty("RootBean");
		}else {
			this.linkFx = new SimpleStringProperty(dbManager.getFolderName(link));
		}
		this.sendTimeFx = new SimpleStringProperty(sendTime);
		this.receiveTimeFx = new SimpleStringProperty(receiveTime);
		this.priorty = new SimpleIntegerProperty(priorty);
	}
	

	public void setEmailIDFx(int emailID) {
		this.emailIDFx.set(emailID);
	}
	
	public int getEmailIDFx() {
		return emailIDFx.get();
	}
	
	public IntegerProperty getEmailIDFxProperty() {
		return emailIDFx;
	}
	
	public void setFromEmailFx(String fromEmail) {
		this.fromEmailFx.set(fromEmail);
	}
	
	public String getFromEmailFx() {
		return fromEmailFx.get();
	}
	
	public StringProperty getFromEmailFxProperty() {
		return fromEmailFx;
	}
	
	public void setToEmailFx(List<String> toEmail) {
		this.toEmailFx.set(toEmail.toString());
	}
	
	public List<String> getToEmailFx() {
		List<String> data = Arrays.asList(toEmailFx.get());
		return data;
	}
	
	public StringProperty getToEmailFxProperty() {
		return toEmailFx;
	}
	
	public void setCcEmailFx(List<String> ccEmail) {
		this.ccEmailFx.set(ccEmail.toString());
	}
	
	public List<String> getCcEmailFx(){
		List<String> data = Arrays.asList(ccEmailFx.get());
		return data;
	}
	
	public StringProperty getCcEmailFxProperty() {
		return ccEmailFx;
	}
	
	public void setBccEmailFx(List<String> bccEmail) {
		this.bccEmailFx.set(bccEmail.toString());
	}
	
	public List<String> getBccEmailFx(){
		List<String> data = Arrays.asList(bccEmailFx.get());
		return data;
	}
	
	public StringProperty getBccEmailFxProperty() {
		return bccEmailFx;
	}
	
	public void setSubjectFx(String subject) {
		this.subjectFx.set(subject);
	}
	
	public String getsubjectFx() {
		return subjectFx.get();
	}
	
	public StringProperty getsubjectFxProperty() {
		return subjectFx;
	}
	
	public void setTextMessageFx(List<String> textMessage) {
		this.textMessageFx.set(textMessage.toString());
	}
	
	public List<String> getTextMessageFx(){
		List<String> data = Arrays.asList(textMessageFx.get());
		return data;
	}
	
	public StringProperty getTextMessageFxProperty() {
		return textMessageFx;
	}
	
	public void setHtmlMessageFx(List<String> htmlMessage) {
		this.htmlMessageFx.set(htmlMessage.toString());
	}
	
	public List<String> getHtmlMessageFx(){
		List<String> data = Arrays.asList(htmlMessageFx.get());
		return data;
	}
	
	public StringProperty getHtmlMessageFxProperty() {
		return htmlMessageFx;
	}
	
	public void setAttachmentFx(List<AttachmentObj> attachment) {
		this.attachmentFx.set(attachment.toString());
	}
	
	public List<String> getAttachmentFx(){
		List<String> data = Arrays.asList(attachmentFx.get());
		return data;
	}
	 
	public StringProperty getAttachmentFxProperty() {
		return attachmentFx;
	}
	
	public void setTypeOfMessageFx(String typeOfMessage) {
		this.typeOfMessageFx.set(typeOfMessage);
	}
	
	public String getTypeOfMessageFx() {
		return typeOfMessageFx.get();
	}
	
	public StringProperty getTypeOfMessageFxProperty() {
		return typeOfMessageFx;
	}
	
	public void setLinkFx(int link) throws SQLException {
		this.linkFx.set(dbManager.getFolderName(link));
	}
	
	public String getLinkFx() {
		return linkFx.get();
	}
	
	public StringProperty getLinkFxProperty() {
		return linkFx;
	}	

	public void setsendTimeFx(String sendTime) {
		this.sendTimeFx.set(sendTime);
	}
	
	public String getSendTimeFx() {
		return sendTimeFx.get();
	}
	
	public StringProperty getSendTimeFxProperty() {
		return sendTimeFx;
	}

	public void setReceiveTimeFx(String receiveTime) {
		this.receiveTimeFx.set(receiveTime);
	}
	
	public String getReceiveTimeFx() {
		return receiveTimeFx.get();
	}
	
	public StringProperty getReceiveTimeFxProperty() {
		return receiveTimeFx;
	}
	
	public void setPriorty(int priority) {
		this.priorty.set(priority);
	}
	
	public int getPriorty() {
		return priorty.get();
	}
	
	public IntegerProperty getPriortyProperty() {
		return priorty;
	}
	
	/**
	 * Sets up the dbManager off the properties file
	 */
	public void setDbManager() {
    	Path propertiespath = Paths.get("jag.properties");
    	if (Files.exists(propertiespath)) {
    		try {
    			Properties prop = new Properties();
				InputStream propInput = new FileInputStream("jag.properties");
				prop.load(propInput);
				this.dbManager = new DatabaseController(prop);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				LOG.debug("Cannot read properties file");
				e.printStackTrace();
			}
    	}else {
    		LOG.debug("Cannot find properties file");
    		//make new props file
       	}
    }
}
