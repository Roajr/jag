package data;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
public class FolderBeanFx {
	
	private IntegerProperty id;
	private StringProperty folderName;
	
	public FolderBeanFx(int id, String folderName) {
		super();
		this.id = new SimpleIntegerProperty(id);
		this.folderName = new SimpleStringProperty(folderName);
	}
	
	public FolderBeanFx() {
		this(-1, "");
	}
	
	public int getId() {
		return id.get();
	}
	
	public void setId(int id) {
		 this.id.set(id);
	}
	
	public IntegerProperty idProperty() {
		return id;
	}
	
	public String getFolderName() {
		return folderName.get();
	}
	
	public void setFolderName(String folderName) {
		this.folderName.set(folderName);
	}
	
	public StringProperty folderNameProperty() {
		return folderName;
	}
}
