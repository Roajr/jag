package data;

import java.io.Serializable;
import java.util.List;

public class EmailBean implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	//unique id of email
	private int emailID;
	private String fromEmail;
	private List<String> toEmail;
	private List<String> ccEmail;
	private List<String> bccEmail;
	private String subject;
	private List<String> textMessage;
	private List<String> htmlMessage;
	private List<AttachmentObj> attachment;
	private String typeOfMessage;
	private int link;
	private String sendTime;
	private String receiveTime;
	/*	
	NO_PRIORITY 		-1
	PRIORITY_HIGH 		1
	PRIORITY_HIGHEST 	2
	PRIORITY_LOW 		3
	PRIORITY_LOWEST 	4	
	PRIORITY_NORMAL 	5
	 */
	private int priorty;
	/*
	 * default constructor
	 */
	public EmailBean() {
		this(-1, "", null, null, null, "", null, null, null, "", -1, "", "", -1);
	}
	
	/*
	 * Non-default constructor
	 * 
	 * @param emailID
	 * @param fromEmail
	 * @param fromEmailPwd
	 * @param toEmail
	 * @param ccEmail
	 * @param bccEmail
	 * @param subject
	 * @param textMessage
	 * @param htmlMessage
	 * @param attachment
	 * @param embeddedAttachment
	 * @param typeOfMessage
	 * @param link
	 * @param sendTime
	 * @param receiveTime
	 * @param priorty
	 */
	public EmailBean(int emailID, String fromEmail, List<String> toEmail, List<String> ccEmail, 
			List<String> bccEmail, String subject, List<String> textMessage, List<String> htmlMessage, 
			List<AttachmentObj> attachment, String typeOfMessage, int link, String sendTime, String receiveTime, 
			int priorty) {
		super();
		this.emailID = emailID;
		this.fromEmail = fromEmail;
		this.toEmail = toEmail;
		this.ccEmail = ccEmail;
		this.bccEmail = bccEmail;
		this.subject = subject;
		this.textMessage = textMessage;
		this.htmlMessage = htmlMessage;
		this.attachment = attachment;
		this.typeOfMessage = typeOfMessage;
		this.link = link;
		this.sendTime = sendTime;
		this.receiveTime = receiveTime;
		this.priorty = priorty;
		
	}
	
	public int getEmailID() {
		return emailID;
	}
	
	public void setEmailID(int emailID) {
		this.emailID = emailID;
	}
	
	public String getFromEmail() {
		return fromEmail;
	}
	
	public void setFromEmail(String fromEmail) {
		this.fromEmail = fromEmail;
	}
	
	public List<String> getToEmail() {
		return toEmail;
	}
	
	public void setToEmail(List<String> toEmail) {
		this.toEmail = toEmail;
	}
	
	public List<String> getCcEmail() {
		return ccEmail;
	}
	
	public void setCcEmail(List<String> ccEmail) {
		this.ccEmail = ccEmail;
	}
	
	public List<String> getBccEmail() {
		return bccEmail;
	}
	
	public void setBccEmail(List<String> bccEmail) {
		this.bccEmail = bccEmail;
	}
	
	public String getSubject() {
		return subject;
	}
	
	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	public List<String> getTextMessage() {
		return textMessage;
	}
	
	public void setTextMessage(List<String> textMessage) {
		this.textMessage = textMessage;
	}
	
	public List<String> getHtmlMessage() {
		return htmlMessage;
	}
	
	public void setHtmlMessage(List<String> htmlMessage) {
		this.htmlMessage = htmlMessage;
	}
	
	public List<AttachmentObj> getAttachment() {
		return attachment;
	}
	
	public void setAttachment(List<AttachmentObj> attachment) {
		this.attachment = attachment;
	}
	
	public String getTypeOfMessage() {
		return typeOfMessage;
	}
	
	public void setTypeOfMessage(String typeOfMessage) {
		this.typeOfMessage = typeOfMessage;
	}
	
	public int getLink() {
		return link;
	}
	
	public void setLink(int link) {
		this.link = link;
	}
	
	public String getSendTime() {
		return sendTime;
	}
	
	public void setSendTime(String sendTime) {
		this.sendTime = sendTime;
	}
	
	public String getReceiveTime() {
		return receiveTime;
	}
	
	public void setReceiveTime(String receiveTime) {
		this.receiveTime = receiveTime;
	}
	
	public int getPriorty() {
		return priorty;
	}
	
	public void setPriorty(int priorty) {
		this.priorty = priorty;
	}
		
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((attachment == null) ? 0 : attachment.hashCode());
		result = prime * result + ((bccEmail == null) ? 0 : bccEmail.hashCode());
		result = prime * result + ((ccEmail == null) ? 0 : ccEmail.hashCode());
		result = prime * result + emailID;
		result = prime * result + ((fromEmail == null) ? 0 : fromEmail.hashCode());
		result = prime * result + ((htmlMessage == null) ? 0 : htmlMessage.hashCode());
		result = prime * result + link;
		result = prime * result + priorty;
		result = prime * result + ((receiveTime == null) ? 0 : receiveTime.hashCode());
		result = prime * result + ((sendTime == null) ? 0 : sendTime.hashCode());
		result = prime * result + ((subject == null) ? 0 : subject.hashCode());
		result = prime * result + ((textMessage == null) ? 0 : textMessage.hashCode());
		result = prime * result + ((toEmail == null) ? 0 : toEmail.hashCode());
		result = prime * result + ((typeOfMessage == null) ? 0 : typeOfMessage.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmailBean other = (EmailBean) obj;
		if (attachment == null) {
			if (other.attachment != null)
				return false;
		} else if (!attachment.equals(other.attachment))
			return false;
		if (bccEmail == null) {
			if (other.bccEmail != null)
				return false;
		} else if (!bccEmail.equals(other.bccEmail))
			return false;
		if (ccEmail == null) {
			if (other.ccEmail != null)
				return false;
		} else if (!ccEmail.equals(other.ccEmail))
			return false;
		if (emailID != other.emailID)
			return false;
		if (fromEmail == null) {
			if (other.fromEmail != null)
				return false;
		} else if (!fromEmail.equals(other.fromEmail))
			return false;
		if (htmlMessage == null) {
			if (other.htmlMessage != null)
				return false;
		} else if (!htmlMessage.equals(other.htmlMessage))
			return false;
		if (link != other.link)
			return false;
		if (priorty != other.priorty)
			return false;
		if (receiveTime == null) {
			if (other.receiveTime != null)
				return false;
		} else if (!receiveTime.equals(other.receiveTime))
			return false;
		if (sendTime == null) {
			if (other.sendTime != null)
				return false;
		} else if (!sendTime.equals(other.sendTime))
			return false;
		if (subject == null) {
			if (other.subject != null)
				return false;
		} else if (!subject.equals(other.subject))
			return false;
		if (textMessage == null) {
			if (other.textMessage != null)
				return false;
		} else if (!textMessage.equals(other.textMessage))
			return false;
		if (toEmail == null) {
			if (other.toEmail != null)
				return false;
		} else if (!toEmail.equals(other.toEmail))
			return false;
		if (typeOfMessage == null) {
			if (other.typeOfMessage != null)
				return false;
		} else if (!typeOfMessage.equals(other.typeOfMessage))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "emailBean [emailID=" + emailID + ", fromEmail=" + fromEmail + ", toEmail=" + toEmail + ", ccEmail="
				+ ccEmail + ", bccEmail=" + bccEmail + ", subject=" + subject + ", textMessage=" + textMessage
				+ ", htmlMessage=" + htmlMessage + ", attachment=" + attachment + ", typeOfMessage=" + typeOfMessage
				+ ", link=" + link + ", sendTime=" + sendTime + ", receiveTime=" + receiveTime + ", priorty=" + priorty
				+ "]";
	}

}
