package data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class WebBeanFx {
	
	private String header = "<!DOCTYPE html><html><head><meta charset=\"ISO-8859-1\"><title>Email Preview</title></head><body>"; 
	private String footer = "</body></html>";

	private StringProperty body;
	private StringProperty subject;
	
	/**
	 * no param constructor
	 */
	public WebBeanFx() {
		this("Please Select a Email", "Select Email");
	}
	
	/**
	 * default constructor
	 * @param body
	 * @param subject
	 */
	public WebBeanFx(String body, String subject) {
		body=body.replaceAll("cid:", "");
		this.body = new SimpleStringProperty(body);
	}
	
	public String getBody() {
		return header + body.get() + footer;
	}
	
	public void setBody(String body) {
		this.body.set(body);
	}
	
	public StringProperty bodyProperty() {
		return body;
	}
	
	public String getSubject() {
		return subject.get();
	}
	
	public void setSubjecty(String subject) {
		this.subject.set(subject);
	}
	
	public StringProperty subjectProperty() {
		return subject;
	}
}
