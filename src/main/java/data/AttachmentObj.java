package data;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import business.MailController;

/**
 * @author Jamroa
 *
 */
/**
 * @author Jamroa
 *
 */
public class AttachmentObj implements Serializable{
    private final static Logger LOG = LoggerFactory.getLogger(MailController.class);
	private static final long serialVersionUID = 1L;
	private String name;
	private String type;
	private byte[] file;
	
	/**
	 * no param constructor
	 */
	public AttachmentObj() {
		this.name = "";
		this.type = "";
		this.file = null;
	}
	
	/**
	 * default constructor of a path
	 * @param name
	 * @param type
	 * @param refrenceLibary
	 * @throws IOException
	 */
	public AttachmentObj(String name, String type, String refrenceLibary) throws IOException {
		this.name = name;
		this.type = type;
		this.file = getByteArray(name, type, refrenceLibary);
	}
	
	/**
	 * default constructor of a byte array
	 * @param name
	 * @param type
	 * @param file
	 */
	public AttachmentObj(String name, String type, byte[] file) {
		this.name = name;
		this.type = type;
		this.file = file;
	}
	
	/**
	 * default constructor of file
	 * @param name
	 * @param filePath
	 * @throws IOException
	 */
	public AttachmentObj(String name, String filePath) throws IOException {
		this.name = name.substring(0, name.indexOf("."));
		this.type = name.substring(name.indexOf("."), name.length());;
		this.file = getByteArray(filePath);
	}
	
	/**
	 * creates the byte array given the files name type and ther path
	 * @param name
	 * @param type
	 * @param refrenceLibary
	 * @return
	 * @throws IOException
	 */
	private byte[] getByteArray(String name, String type, String refrenceLibary) throws IOException {
		File file = new File(name.concat(".").concat(type));
		byte[] byteArray = Files.readAllBytes(file.toPath());
		return byteArray;
	}
	
	/**
	 * will get a byte array for the image based on the file
	 * @param filePath
	 * @return
	 * @throws IOException
	 */
	private byte[] getByteArray(String filePath) throws IOException {
		File file = new File(filePath);
		byte[] byteArray = Files.readAllBytes(file.toPath());
		return byteArray;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public byte[] getFile() {
		return file;
	}

	public void setFile(byte[] file) {
		this.file = file;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(file);
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AttachmentObj other = (AttachmentObj) obj;
		if (!Arrays.equals(file, other.file))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}
	
}
