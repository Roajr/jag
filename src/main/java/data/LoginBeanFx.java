/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * @author Jamroa
 */
public class LoginBeanFx {
    private StringProperty emailAdd;
    private StringProperty emailPwd;
    private StringProperty dbUser;
    private StringProperty dbPwd;
    private StringProperty db;
    
    public LoginBeanFx(final String emailAdd, final String emailPwd, final String dbUser, final String dbPwd, String db){
        super();
        this.emailAdd = new SimpleStringProperty(emailAdd);
        this.emailPwd = new SimpleStringProperty(emailPwd);
        this.dbUser = new SimpleStringProperty(dbUser);
        this.dbPwd = new SimpleStringProperty(dbPwd);
        this.db = new SimpleStringProperty(db);

    }
    
    public LoginBeanFx(){
        this("","","","","");
    }
    
    public String getEmailAdd(){
        return emailAdd.get();
    }
    public void setEmailAdd(String emailAdd){
        this.emailAdd.set(emailAdd);
    }
    public StringProperty emailAddProperty(){
        return emailAdd;
    }
    
    public String getEmailPwd(){
        return emailPwd.get();
    }
    public void setEmailPwd(String emailPwd){
        this.emailPwd.set(emailPwd);
    }
    public StringProperty emailPwdProperty(){
        return emailPwd;
    }
    
    public String getDbUser(){
        return dbUser.get();
    }
    public void setDbUser(String dbUser){
        this.dbUser.set(dbUser);
    }
    public StringProperty dbUserProperty(){
        return dbUser;
    }
    
    public String getDbPwd(){
        return dbPwd.get();
    }
    public void setDbPwd(String dbPwd){
        this.dbPwd.set(dbPwd);
    }
    public StringProperty dbPwdProperty(){
        return dbPwd;
    }
    
    public String getDb(){
        return db.get();
    }
    public void setDb(String db){
        this.db.set(db);
    }
    public StringProperty dbProperty(){
        return db;
    }
    
    
}
