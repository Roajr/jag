package dataTest;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import business.DatabaseController;
import business.MailController;
import data.AttachmentObj;
import data.EmailBean;
import junit.framework.TestCase;

public class EmailBeanTest extends TestCase {
	
    private final static Logger LOG = LoggerFactory.getLogger(MailController.class);
	EmailBean email = new EmailBean();
	private MailController emailControl;
	private DatabaseController dbManager;
    private Properties prop;
    private InputStream propInput;
	
	@Before
	public void setUp() {
		Path propertiespath = Paths.get("jag.properties");
    	if (Files.exists(propertiespath)) {
    		try {
    			prop = new Properties();
				this.propInput = new FileInputStream("jag.properties");
				this.prop.load(propInput);
				dbManager = new DatabaseController(prop);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				LOG.info("Cannot read properties file");
				e.printStackTrace();
			}
    	}else {
			LOG.info("properties file does noe existe");
    	}
		try{
			String refrenceLibary = "src/test/resources/";
			int emailID = -1;
			String fromEmail = "send.1439367@gmail.com";
			List<String> toEmail = new ArrayList<String>();
				toEmail.add("send.1439367@gmail.com");
				toEmail.add("receive.1439367@gmail.com");
				toEmail.add("other.1439367@gmail.com");
			List<String> ccEmail = new ArrayList<String>();
				ccEmail.add("send.1439367@gmail.com");
				ccEmail.add("receive.1439367@gmail.com");
				ccEmail.add("other.1439367@gmail.com");
			List<String> bccEmail = new ArrayList<String>();
				bccEmail.add("send.1439367@gmail.com");
				bccEmail.add("receive.1439367@gmail.com");
				bccEmail.add("other.1439367@gmail.com");  
			String subject = "TestEmail";
			List<String> textMessage = new ArrayList<String>();
				textMessage.add("UTF-8");
				textMessage.add("TEXT/PLAIN");
				textMessage.add("This is the message body");
			List<String> htmlMessage = new ArrayList<String>();
				htmlMessage.add("UTF-8");
				htmlMessage.add("TEXT/HTML"); 
				htmlMessage.add("<h1>This is a test Email</h1><p>This is a embedded photo</p><img src='cid:FreeFall.jpg'><h2>There should be a attached photo</h2>");
			List<AttachmentObj> attachment = new ArrayList<AttachmentObj>();
				attachment.add(new AttachmentObj("WindsorKen180","jpg",prop.getProperty("refrenceLibary")));
				attachment.add(new AttachmentObj("FreeFall","jpg",prop.getProperty("refrenceLibary")));
			String typeOfMessage = "new";
			int link = 1;
			String sendTime = LocalDateTime.now().withSecond(0).withNano(0).toString();
			String receiveTime = "";
			int priorty = 5;
			email = new EmailBean(emailID, fromEmail, toEmail, ccEmail, bccEmail, subject, textMessage, htmlMessage, 
					attachment, typeOfMessage, link, sendTime, receiveTime, priorty);	
			emailControl = new MailController(email);
			dbManager.createDatabaseTables();
		} catch (IOException e) {
			e.printStackTrace();
			LOG.info("Something went wrong with the IO of attachments");
		} catch (SQLException e) {
			LOG.info("There was a error with the creation of dbManager");
			e.printStackTrace();
		}
	}

	//Tests to validate if the Bean Saved the valid values
	@Test
	public void testBeanEmailID() {
		assertEquals(-1, email.getEmailID());
	}
	@Test
	public void testBeanFrom() {
		assertEquals("send.1439367@gmail.com", email.getFromEmail());
	}
	@Test
	public void testBeanTo1() {
		assertEquals("send.1439367@gmail.com", email.getToEmail().get(0).toString());
	}
	@Test
	public void testBeanTo2() {
		assertEquals("receive.1439367@gmail.com", email.getToEmail().get(1).toString());
	}
	@Test
	public void testBeanTo3() {
		assertEquals("other.1439367@gmail.com", email.getToEmail().get(2).toString());
	}
	@Test
	public void testBeanCc1() {
		assertEquals("send.1439367@gmail.com", email.getCcEmail().get(0).toString());
	}
	@Test
	public void testBeanCc2() {
		assertEquals("receive.1439367@gmail.com", email.getCcEmail().get(1).toString());
	}@Test
	public void testBeanCc3() {
		assertEquals("other.1439367@gmail.com", email.getCcEmail().get(2).toString());
	}
	@Test
	public void testBeanBcc1() {
		assertEquals("send.1439367@gmail.com", email.getBccEmail().get(0).toString());
	}
	@Test
	public void testBeanBcc2() {
		assertEquals("receive.1439367@gmail.com", email.getBccEmail().get(1).toString());
	}
	@Test
	public void testBeanBcc3() {
		assertEquals("other.1439367@gmail.com", email.getBccEmail().get(2).toString());
	}
	@Test
	public void testBeanSubject() {
		assertEquals("TestEmail", email.getSubject());
	}
	@Test
	public void testBeanTextMessage() {
		assertEquals("This is the message body", email.getTextMessage().get(2).toString());
	}
	@Test
	public void testBeanhtmlMessage() {
		assertEquals("<h1>This is a test Email</h1><p>This is a embedded photo</p><img src='cid:FreeFall.jpg'><h2>There should be a attached photo</h2>", email.getHtmlMessage().get(2).toString());
	}
	@Test
	public void testBeanAttachment1file() {
		AttachmentObj testattach = null;
		try {
			testattach = new AttachmentObj("WindsorKen180","jpg",prop.getProperty("refrenceLibary"));
		} catch (IOException e) {
			LOG.info("Creating the test object failed");
			e.printStackTrace();
		}
		assertEquals(email.getAttachment().get(0), testattach);
	}
	@Test
	public void testBeanAttachment2file() {
		AttachmentObj testattach = null;
		try {
			testattach = new AttachmentObj("FreeFall","jpg",prop.getProperty("refrenceLibary"));
		} catch (IOException e) {
			LOG.info("Creating the test object failed");
			e.printStackTrace();
		}
		assertEquals(email.getAttachment().get(1), testattach);
	}
	@Test
	public void testBeanTypeOfMessage() {
		assertEquals("new",email.getTypeOfMessage());
	}
	@Test
	public void testBeanLink() {
		assertEquals(1,email.getLink());
	} 
	@Test
	public void testBeanSendTime() {
		assertEquals(LocalDateTime.now().withSecond(0).withNano(0).toString(),email.getSendTime());
	}
	@Test
	public void testBeanReceiveTime() {
		assertEquals("",email.getReceiveTime());
	}
	@Test
	public void testBeanPriorty() {
		assertEquals(5,email.getPriorty());
	}
	@Test
	public void testSendEmail() throws SQLException {
		boolean emailSent = emailControl.sendEmail(email.getFromEmail(), prop.getProperty("send"), email);
		assertEquals(true, emailSent);
	}
	@Test
	public void testSetMethods() throws IOException {
		EmailBean testBean = new EmailBean();
		String fromEmail = "send.1439367@gmail.com";
		testBean.setFromEmail("send.1439367@gmail.com");
		List<String> toEmail = new ArrayList<String>();
			toEmail.add("send.1439367@gmail.com");
			toEmail.add("receive.1439367@gmail.com");
			toEmail.add("other.1439367@gmail.com");
		testBean.setToEmail(toEmail);
		List<String> ccEmail = new ArrayList<String>();
			ccEmail.add("send.1439367@gmail.com");
			ccEmail.add("receive.1439367@gmail.com");
			ccEmail.add("other.1439367@gmail.com");
		testBean.setCcEmail(ccEmail);
		List<String> bccEmail = new ArrayList<String>();
			bccEmail.add("send.1439367@gmail.com");
			bccEmail.add("receive.1439367@gmail.com");
			bccEmail.add("other.1439367@gmail.com");  
		testBean.setBccEmail(bccEmail);
		String subject = "TestEmail";
		testBean.setSubject(subject);
		List<String> textMessage = new ArrayList<String>();
			textMessage.add("UTF-8");
			textMessage.add("TEXT/PLAIN");
			textMessage.add("This is the message body");
		testBean.setTextMessage(textMessage);
		List<String> htmlMessage = new ArrayList<String>();
			htmlMessage.add("UTF-8");
			htmlMessage.add("TEXT/HTML"); 
			htmlMessage.add("<h1>This is a test Email</h1><p>This is a embedded photo</p><img src='cid:FreeFall.jpg'><h2>There should be a attached photo</h2>");
		testBean.setHtmlMessage(htmlMessage);
		List<AttachmentObj> attachment = new ArrayList<AttachmentObj>();
			attachment.add(new AttachmentObj("WindsorKen180","jpg",prop.getProperty("refrenceLibary")));
			attachment.add(new AttachmentObj("FreeFall","jpg",prop.getProperty("refrenceLibary")));
		testBean.setAttachment(attachment);
		String typeOfMessage = "new";
		testBean.setTypeOfMessage(typeOfMessage);
		int link = 1;
		testBean.setLink(link);
		String sendTime = LocalDateTime.now().withSecond(0).withNano(0).toString();
		testBean.setSendTime(sendTime);
		String receiveTime = "";
		testBean.setReceiveTime(receiveTime);
		int priorty = 5;
		testBean.setPriorty(priorty);
		assertTrue(email.equals(testBean));
	}
	//Tests to validate the received email
	@Test
	public void testReceiveEmailFrom() throws SQLException, IOException {
		List<EmailBean> receivedEmails = new ArrayList<EmailBean>();
		boolean emailSent = emailControl.sendEmail(email.getFromEmail(), prop.getProperty("send"), email);
		if(emailSent) {
			try {
				Thread.sleep(5000);
				receivedEmails = emailControl.receiveMail(email.getToEmail().get(1), prop.getProperty("receive"));
				assertEquals(receivedEmails.get(0).getFromEmail() ,email.getFromEmail());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}else {
			LOG.info("The Email failed to send");
		}
		
		//Removing things that can not be the same
		email.setReceiveTime("");
		email.setSendTime("");
		email.setBccEmail(new ArrayList<String>());
		email.setLink(-1);
		receivedEmails.get(0).setReceiveTime("");
		receivedEmails.get(0).setSendTime("");
		receivedEmails.get(0).setBccEmail(new ArrayList<String>());
		assertTrue(email.equals(receivedEmails.get(0)));
	}
}	

