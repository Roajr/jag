package dataTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import business.DatabaseController;
import business.MailController;
import data.AttachmentObj;
import data.EmailBean;

public class EmailDBTest {
	
    private final static Logger LOG = LoggerFactory.getLogger(MailController.class);
	EmailBean email = new EmailBean();
	private MailController emailControl;
	private DatabaseController dbManager;
    private Properties prop;
    private InputStream propInput;
	
	@Before
	public void setUp() {
		Path propertiespath = Paths.get("jag.properties");
    	if (Files.exists(propertiespath)) {
    		try {
    			prop = new Properties();
				this.propInput = new FileInputStream("jag.properties");
				this.prop.load(propInput);
				dbManager = new DatabaseController(prop);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				LOG.info("Cannot read properties file");
				e.printStackTrace();
			}
    	}else {
			LOG.info("properties file does noe existe");
    	}
		try{
			String refrenceLibary = "src/test/resources/";
			int emailID = -1;
			String fromEmail = "send.1439367@gmail.com";
			List<String> toEmail = new ArrayList<String>();
				toEmail.add("send.1439367@gmail.com");
				toEmail.add("receive.1439367@gmail.com");
				toEmail.add("other.1439367@gmail.com");
			List<String> ccEmail = new ArrayList<String>();
				ccEmail.add("send.1439367@gmail.com");
				ccEmail.add("receive.1439367@gmail.com");
				ccEmail.add("other.1439367@gmail.com");
			List<String> bccEmail = new ArrayList<String>();
				bccEmail.add("send.1439367@gmail.com");
				bccEmail.add("receive.1439367@gmail.com");
				bccEmail.add("other.1439367@gmail.com");  
			String subject = "TestEmail";
			List<String> textMessage = new ArrayList<String>();
				textMessage.add("UTF-8");
				textMessage.add("TEXT/PLAIN");
				textMessage.add("This is the message body");
			List<String> htmlMessage = new ArrayList<String>();
				htmlMessage.add("UTF-8");
				htmlMessage.add("TEXT/HTML"); 
				htmlMessage.add("<h1>This is a test Email</h1><p>This is a embedded photo</p><img src='cid:FreeFall.jpg'><h2>There should be a attached photo</h2>");
			List<AttachmentObj> attachment = new ArrayList<AttachmentObj>();
				attachment.add(new AttachmentObj("WindsorKen180","jpg",prop.getProperty("refrenceLibary")));
				attachment.add(new AttachmentObj("FreeFall","jpg",prop.getProperty("refrenceLibary")));
			String typeOfMessage = "new";
			int link = 1;
			String sendTime = LocalDateTime.now().withSecond(0).withNano(0).toString();
			String receiveTime = "";
			int priorty = 5;
			email = new EmailBean(emailID, fromEmail, toEmail, ccEmail, bccEmail, subject, textMessage, htmlMessage, 
					attachment, typeOfMessage, link, sendTime, receiveTime, priorty);	
			emailControl = new MailController(email);
			dbManager.createDatabaseTables();
		} catch (IOException e) {
			e.printStackTrace();
			LOG.info("Something went wrong with the IO of attachments");
		} catch (SQLException e) {
			LOG.info("There was a error with the creation of dbManager");
			e.printStackTrace();
		}
	}
	
	@Test
	public void testDBtablesNumber() throws SQLException {
		int count = 0;
		try(Connection conn = DriverManager.getConnection(prop.getProperty("URL"),prop.getProperty("USER"), prop.getProperty("PASSWORD"))){     	
			Statement stat = conn.createStatement();
			String Query;
			Query = "SHOW TABLES;";
			ResultSet rs = stat.executeQuery(Query);
	   		while(rs.next()) {
	   			count++;
	   		}
	   	}
	   	assertEquals(count, 5);
	}
	@Test
	public void testDBtablesemailbean() throws SQLException {
		dbManager.create(email);
		int emailID = 0;
	   	try(Connection conn = DriverManager.getConnection(prop.getProperty("URL"),prop.getProperty("USER"), prop.getProperty("PASSWORD"))){     	
	   		Statement stat = conn.createStatement();
	   		String Query;
	  		Query = "Select * from emailbean";
	   		ResultSet rs = stat.executeQuery(Query);
	   		while(rs.next()) {
	   			emailID = rs.getInt(1);
	   		}
	   	}
	   	assertEquals(emailID, 1);
	}
	@Test
	public void testDBCfolderDefault() throws SQLException {
		String test = "";
		dbManager.create(email);
		try(Connection conn = DriverManager.getConnection(prop.getProperty("URL"),prop.getProperty("USER"), prop.getProperty("PASSWORD"))){     	
	   		Statement stat = conn.createStatement();
	   		String Query;
	   		Query = "Select folderName from folderstable";
	   		ResultSet rs = stat.executeQuery(Query);
	   		while(rs.next()) {
	   			test = test + rs.getString(1);
	   		}
	   	}
		assertEquals(test, "sentinbox");
	}
	@Test
	public void testemailBeanadd() throws SQLException {
		dbManager.create(email);
		dbManager.create(email);
		dbManager.create(email);
		dbManager.create(email);
		dbManager.create(email);
		int idCount = 0;
		try(Connection conn = DriverManager.getConnection(prop.getProperty("URL"),prop.getProperty("USER"), prop.getProperty("PASSWORD"))){     	
	   		Statement stat = conn.createStatement();
	   		String Query;
	   		Query = "Select count(emailID) from emailbean";
	   		ResultSet rs = stat.executeQuery(Query);
	   		while(rs.next()) {
	   			idCount = rs.getInt(1);
	   		}
	   	}
		assertEquals(idCount, 5);
	}
	@Test
	public void testemailBeanAutoIndex() throws SQLException {
		dbManager.create(email);
		dbManager.create(email);
		dbManager.create(email);
		dbManager.create(email);
		dbManager.create(email);
		int idCount = 0;
		try(Connection conn = DriverManager.getConnection(prop.getProperty("URL"),prop.getProperty("USER"), prop.getProperty("PASSWORD"))){     	
	   		Statement stat = conn.createStatement();
	   		String Query;
	   		Query = "Select max(emailID) from emailbean";
	   		ResultSet rs = stat.executeQuery(Query);
	   		while(rs.next()) {
	   			idCount = rs.getInt(1);
	   		}
	   	}
		assertEquals(idCount, 5);
	}
	@Test
	public void testemailBeanDelete() throws SQLException {
		dbManager.create(email);
		dbManager.create(email);
		dbManager.create(email);
		dbManager.create(email);
		dbManager.create(email);
		dbManager.delete(2);
		int idCount = 0;
		try(Connection conn = DriverManager.getConnection(prop.getProperty("URL"),prop.getProperty("USER"), prop.getProperty("PASSWORD"))){     	
	   		Statement stat = conn.createStatement();
	   		String Query;
	   		Query = "Select max(emailID) from emailbean";
	   		ResultSet rs = stat.executeQuery(Query);
	   		while(rs.next()) {
	   			idCount = rs.getInt(1);
	   		}
	   	}
		assertEquals(idCount, 5);
	}
	@Test
	public void testemailBeanDeleteRow() throws SQLException {
		dbManager.create(email);
		dbManager.create(email);
		dbManager.create(email);
		dbManager.create(email);
		dbManager.create(email);
		dbManager.delete(2);
		int idCount = 0;
		try(Connection conn = DriverManager.getConnection(prop.getProperty("URL"),prop.getProperty("USER"), prop.getProperty("PASSWORD"))){     	
	   		Statement stat = conn.createStatement();
	   		String Query;
	   		Query = "Select count(emailID) from emailbean";
	   		ResultSet rs = stat.executeQuery(Query);
	   		while(rs.next()) {
	   			idCount = rs.getInt(1);
	   		}
	   	}
		assertEquals(idCount, 4);
	}
	@Test
	public void testemailBeanCheckDefaultFolder() throws SQLException {
		dbManager.create(email);
		int folderID = 0;
		try(Connection conn = DriverManager.getConnection(prop.getProperty("URL"),prop.getProperty("USER"), prop.getProperty("PASSWORD"))){     	
	   		Statement stat = conn.createStatement();
	   		String Query;
	   		Query = "Select * from emailbean where emailId=1";
	   		ResultSet rs = stat.executeQuery(Query);
	   		while(rs.next()) {
	   			folderID = rs.getInt(8);
	   		}
	   	}
		assertEquals(folderID, 1);
	}
	@Test
	public void testemailBeanCheckChangeFolder() throws SQLException {
		dbManager.create(email);
		dbManager.changeFolder(1, "inbox");
		int folderID = 0;
		try(Connection conn = DriverManager.getConnection(prop.getProperty("URL"),prop.getProperty("USER"), prop.getProperty("PASSWORD"))){     	
	   		Statement stat = conn.createStatement();
	   		String Query;
	   		Query = "Select * from emailbean where emailId=1";
	   		ResultSet rs = stat.executeQuery(Query);
	   		while(rs.next()) {
	   			folderID = rs.getInt(8);
	   		}
	   	}
		assertEquals(folderID, 2);
	}
	@Test
	public void testemailBeanCheckChangeFolderDoesntExiste() throws SQLException {
		dbManager.create(email);
		dbManager.changeFolder(1, "newFolder");
		int folderID = 0;
		try(Connection conn = DriverManager.getConnection(prop.getProperty("URL"),prop.getProperty("USER"), prop.getProperty("PASSWORD"))){     	
	   		Statement stat = conn.createStatement();
	   		String Query;
	   		Query = "Select * from emailbean where emailId=1";
	   		ResultSet rs = stat.executeQuery(Query);
	   		while(rs.next()) {
	   			folderID = rs.getInt(8);
	   		}
	   	}
		assertEquals(folderID, 3);
	}
	@Test
	public void testemailBeanCheckChangeFolderDoesntExisteName() throws SQLException {
		dbManager.create(email);
		dbManager.changeFolder(1, "newFolder");
		String folderName = "";
		try(Connection conn = DriverManager.getConnection(prop.getProperty("URL"),prop.getProperty("USER"), prop.getProperty("PASSWORD"))){     	
	   		Statement stat = conn.createStatement();
	   		String Query;
	   		Query = "Select folderName from folderstable where folderID = 3";
	   		ResultSet rs = stat.executeQuery(Query);
	   		while(rs.next()) {
	   			folderName = rs.getString(1);
	   			LOG.info(folderName);
	   		}
	   	}
		assertEquals(folderName, "newFolder");
	}
	@Test
	public void testemailBeanCheckChangeNewFolder() throws SQLException {
		dbManager.create(email);
		dbManager.changeNewFolder(1, "test");
		int folderID = 0;
		try(Connection conn = DriverManager.getConnection(prop.getProperty("URL"),prop.getProperty("USER"), prop.getProperty("PASSWORD"))){     	
	   		Statement stat = conn.createStatement();
	   		String Query;
	   		Query = "Select * from emailbean where emailId=1";
	   		ResultSet rs = stat.executeQuery(Query);
	   		while(rs.next()) {
	   			folderID = rs.getInt(8);
	   		}
	   	}
		assertEquals(folderID, 3);
	}
	@Test
	public void testemailBeanCheckChangeNewFolderName() throws SQLException {
		dbManager.create(email);
		dbManager.changeNewFolder(1, "test");
		String folderName = "";
		try(Connection conn = DriverManager.getConnection(prop.getProperty("URL"),prop.getProperty("USER"), prop.getProperty("PASSWORD"))){     	
	   		Statement stat = conn.createStatement();
	   		String Query;
	   		Query = "Select folderName from folderstable where folderID = 3";
	   		ResultSet rs = stat.executeQuery(Query);
	   		while(rs.next()) {
	   			folderName = rs.getString(1);
	   		}
	   	}
		assertEquals(folderName, "test");
	}
	@Test
	public void testemailBeanGetBean() throws SQLException {
		dbManager.create(email);
		EmailBean testEmail = dbManager.findID(1);
		//this is to act like it is placec in the database
		email.setEmailID(1);
		LOG.info(email.toString());
		LOG.info(testEmail.toString());
		assertTrue(email.equals(testEmail));
	}
	@Test
	public void testemailBeanGetBeanAll() throws SQLException {
		dbManager.create(email);
		List<EmailBean> testEmail = dbManager.findAll();
		//this is to act like it is place in the database
		email.setEmailID(1);
		LOG.info(email.toString());
		LOG.info(testEmail.toString());
		assertTrue(email.equals(testEmail.get(0)));
	}
	@Test
	public void testemailBeanGetBeanAlllist() throws SQLException {
		dbManager.create(email);
		dbManager.create(email);
		dbManager.create(email);
		List<EmailBean> testEmail = dbManager.findAll();
		//this is to act like it is place in the database
		email.setEmailID(3);
		LOG.info(email.toString());
		LOG.info(testEmail.toString());
		assertTrue(email.equals(testEmail.get(2)));
	}
}
